package com.equest.jira.plugin.mail;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.equest.jira.plugin.messagehandler.CommentHandler;
import com.equest.jira.plugin.utility.CustomFieldUtility;

public class WriteMailServlet  extends HttpServlet
{	
		private static final long serialVersionUID = 1L;
		String baseUrl=ComponentAccessor.getApplicationProperties().getString("jira.baseurl");
		String subject ="";
		String to ="";
		String cc ="";
		String body ="";
		String issueKey ="";
		String submit="";
		String  bcc ="";
		String from="";
		EmailUtility emailUtility=null;
		CommentHandler commentHandler =new CommentHandler();
		CustomFieldUtility customFieldUtility = new CustomFieldUtility();

		protected void doPost(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException
	    {  
			issueKey = request.getParameter("issueKey");
			subject = request.getParameter("IssueSubject");
			submit = request.getParameter("Submit");
			
			to = request.getParameter("to");
			cc = request.getParameter("cc");
			bcc = request.getParameter("Bcc");
			from=request.getParameter("from");
			
			body = request.getParameter("body");
		//	System.out.println(" ----------------------Getting Values for mail------------------------");
		//	System.out.println(" Subject : " +subject);
		//	System.out.println(" to : " +to);
		//	System.out.println(" cc : " +cc);
		//	System.out.println(" Bcc : " +bcc);
		//	System.out.println(" body : " +body);
		//	System.out.println(" URL : " +baseUrl+ "/browse/" +issueKey);
		//	System.out.println(" Submit : " +submit);	
		//	System.out.println(" ----------------------Done Getting Values for mail------------------------");
			
			System.out.println(" ----------------------Sending mail------------------------");
			emailUtility = new EmailUtility();		
			emailUtility.createEmail(to, cc, bcc,body, subject,submit,from);
			System.out.println(" ----------------------Mail Sent------------------------");
			
			
			String upadatedMail = "";
			if(from!=null)
			{
				upadatedMail = "....................................................................................................................................." 
											+"\r\n "  + "From : " +from 
											+ "\r\n "+ "Sent : "+ (new SimpleDateFormat("EEE, MMM dd HH:mm:ss yyyy z").format(Calendar.getInstance().getTime())).toString()
											+ "\r\n " + "To : "+to
											+ "\r\n " + "Cc : "+cc;
			}
			
			
			/*if(submit.equals("REPLY ALL") && !(cc.equals("")) )
			{
				upadatedMail = upadatedMail + "\r\n " + "Cc : "+cc;
			}*/
			upadatedMail =  upadatedMail	+ "\r\n "+"Subject : "+subject+ "\r\n " +commentHandler.getupdatedSentMail(issueKey,body);
			commentHandler.updateMailTrail(issueKey, upadatedMail);	
			if(!(to.equals("") || to == null))
			{
				CustomField customfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("To");			
				MutableIssue  issue = ComponentAccessor.getIssueManager().getIssueObject(issueKey);
				customFieldUtility.saveTextValue(issue,customfield, to);
			}
			if(!(cc.equals("") || cc == null))
			{
				CustomField customfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Cc");			
				MutableIssue  issue = ComponentAccessor.getIssueManager().getIssueObject(issueKey);
				customFieldUtility.saveTextValue(issue,customfield, cc);
			}
				
			
		//	response.encodeRedirectURL(baseUrl+ "/browse/" +issueKey);
			response.sendRedirect(baseUrl+ "/browse/" +issueKey);	   	
	    
	     }
	      
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException
	    { 
	    	doPost(request,response);
	    }    
	    
	  }


