package com.equest.jira.plugin.mail;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.mail.Email;
import com.atlassian.mail.MailException;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;

public class EmailUtility {

	Email newEmail = null;
	com.atlassian.mail.server.impl.SMTPMailServerImpl smtpMailServerImpl = null;
	public void createEmail(String to,String cc,String bcc, String body,String subject, String submit,String from)
	{
		if(body != null || to != null)
		{
			newEmail = new Email(to);
			newEmail.setTo(to);
			if(submit.equals("REPLY ALL") && ( !(bcc.equals("")) || !(cc.equals(""))) )
			{
				newEmail.setCc(cc);
				if(!bcc.equals(""))
				{
					newEmail.setBcc(bcc);
				//	System.out.println("----------Bcc Set : ----------------" );
				}
			}
			newEmail.setSubject(subject);	
			newEmail.setBody(body);
			
			newEmail.setMimeType("text/plain");
			
			newEmail.setFrom(from);
		//	newEmail.setFromName("Admin");
		//	newEmail.setMessageId("e06deabc923f3378e4b237a20be324cc");
		
			
			MailServerManager mailServerManager = ComponentAccessor.getMailServerManager();
			SMTPMailServer smtpMailServer = mailServerManager.getDefaultSMTPMailServer();
			
			
			
			try {
			
				/*
				System.out.println("--------------!!!!!!!----------------------- ");
				System.out.println("getId : - " +smtpMailServer.getId());
				System.out.println("getName : - " +smtpMailServer.getName());
				System.out.println("getDescription : - " +smtpMailServer.getDescription());
				System.out.println("getDefaultFrom : - " +smtpMailServer.getDefaultFrom());
				System.out.println("getPrefix : - " +smtpMailServer.getPrefix());	
				System.out.println("getJndiLocation : - " +smtpMailServer.getHostname());
//				System.out.println("getUsername : - " +smtpMailServer.getUsername());
	//			System.out.println("getPassword : - " +smtpMailServer.getPassword());
				System.out.println("--------------!!!!!!!----------------------- ");
				//smtpMailServer.getSession(); // false
				
				*/
				
				smtpMailServerImpl = new com.atlassian.mail.server.impl.SMTPMailServerImpl(smtpMailServer.getId(),smtpMailServer.getName(),smtpMailServer.getDescription(),smtpMailServer.getDefaultFrom(), smtpMailServer.getPrefix(),false,smtpMailServer.getHostname(),smtpMailServer.getUsername(),smtpMailServer.getPassword());
				
				
				
				
				
				  
				
		//		 public SMTPMailServerImpl(Long id, String name, String description, String from, String prefix, boolean isSession, String location, String username, String password)
	//			System.out.println("New Email : "+newEmail.getCc());
	//			System.out.println("New Email : "+newEmail.getBcc());
				
				
				smtpMailServerImpl.send(newEmail);
					
					
	
			} catch (MailException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	
}
