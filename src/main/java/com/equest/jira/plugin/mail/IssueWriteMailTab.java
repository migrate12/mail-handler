package com.equest.jira.plugin.mail;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import java.util.ArrayList;
import java.util.List;

public class IssueWriteMailTab extends AbstractIssueTabPanel
{
  private ActiveObjects ao = null;

  public IssueWriteMailTab(ActiveObjects ao)
  {
    this.ao = ao;
  }

  public List getActions(Issue issue, User user)
  {
    List panelActions = new ArrayList();
    panelActions.add(new WriteMailAction(this.descriptor, issue, user, this.ao));

    return panelActions;
  }

  public boolean showPanel(Issue arg0, User arg1)
  {
    return true;
  }
}