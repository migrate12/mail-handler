package com.equest.jira.plugin.mail;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import com.equest.jira.plugin.utility.SortOperation;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WriteMailAction extends AbstractIssueAction
{
  Issue issueObject = null;
  User userObject = null;
  ActiveObjects ao = null;

  public WriteMailAction(IssueTabPanelModuleDescriptor descriptor, Issue issue, User user, ActiveObjects ao)
  {
    super(descriptor);
    this.issueObject = issue;
    this.userObject = user;
    this.ao = ao;
  }

  public Date getTimePerformed()
  {
    return new Date();
  }

  protected void populateVelocityParams(Map contextMap)
  {
    Map emailIds = new HashMap();

    MailServerManager mailServerManager = ComponentAccessor.getMailServerManager();
    SMTPMailServer smtpMailServer = mailServerManager.getDefaultSMTPMailServer();

    if (this.userObject.getEmailAddress() != null)
      emailIds.put(this.userObject.getEmailAddress(), this.userObject.getEmailAddress());
    if (this.issueObject.getProjectObject().getEmail() != null)
      emailIds.put(this.issueObject.getProjectObject().getEmail(), this.issueObject.getProjectObject().getEmail());
    if (smtpMailServer != null) {
      emailIds.put(smtpMailServer.getDefaultFrom(), smtpMailServer.getDefaultFrom());
    }
    contextMap.put("To", getCustomeFieldValue("To", this.issueObject));

    contextMap.put("Cc", getCustomeFieldValue("Cc", this.issueObject));

    contextMap.put("summary", " Case #  " + this.issueObject.getKey() + " " + this.issueObject.getSummary());
    contextMap.put("baseUrl", ComponentAccessor.getApplicationProperties().getString("jira.baseurl"));
    contextMap.put("issueKey", this.issueObject.getKey());
    if (!emailIds.isEmpty()) {
      contextMap.put("emailIds", SortOperation.byKeyAsce(emailIds));
    }

    contextMap.put("MailTrail", "\r\n \r\n \r\n \r\n " + getCustomeFieldValue("MailTrail", this.issueObject));
  }

  public String getCustomeFieldValue(String customFieldName, Issue issue)
  {
    String customFieldValue = "";
    CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
    CustomField customeField = customFieldManager.getCustomFieldObjectByName(customFieldName);
    if (customeField != null)
    {
      customFieldValue = customeField.getValueFromIssue(issue);
      if (customFieldValue != null) {
        return customFieldValue;
      }
    }
    return "";
  }
}