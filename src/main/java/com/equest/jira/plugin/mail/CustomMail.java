package com.equest.jira.plugin.mail;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;

public class CustomMail extends AbstractJiraContextProvider
{

	@Override
	public Map getContextMap(User user, JiraHelper jiraHelper) {
		
		Map<String,Object> contextMap = new HashMap<String,Object>();
		Issue currentIssue = (Issue) jiraHelper.getContextParams().get("issue");
		
		contextMap.put("To", getCustomeFieldValue("To",currentIssue));
		contextMap.put("Cc", getCustomeFieldValue("Cc",currentIssue));
		contextMap.put("summary", ( "ACL Case #  " + currentIssue.getKey() +" "+currentIssue.getSummary()) );
		contextMap.put("baseUrl",ComponentAccessor.getApplicationProperties().getString("jira.baseurl"));
		contextMap.put("issueKey",currentIssue.getKey());
		
		return contextMap;
	}
	
	public String getCustomeFieldValue(String customFieldName,Issue issue)
	{
		String customFieldValue ="";
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		CustomField customeField = customFieldManager.getCustomFieldObjectByName(customFieldName);
		if(customeField != null)
			customFieldValue = customeField.getValueFromIssue(issue);
	
		return customFieldValue;
	}
	

}
