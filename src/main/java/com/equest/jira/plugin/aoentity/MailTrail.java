package com.equest.jira.plugin.aoentity;

import java.sql.Blob;
import java.sql.Clob;

import com.sun.mail.handlers.text_html;

import net.java.ao.Entity;
import net.java.ao.Preload;
 
@Preload
public interface MailTrail  extends Entity
{
	public String getIssueKey();
	public void setIssueKey(String issueKey);
	
	
	public Blob getMailTrail();
	public void  setMailTrail(Blob mailTrail);
}