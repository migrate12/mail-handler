package com.equest.jira.plugin.aoentity;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;
 
@Preload
public interface Rules extends Entity
{
	
	@AutoIncrement
	@NotNull
	@PrimaryKey("ID")
	public int getID();
	
	public String getUserEmail();
	public void setUserEmail(String userEmail);
	
	public String getSubjectContain();
	public void  setSubjectContain(String subjectContain);
	
	public String getMailForm();
	public void  setMailForm(String mailForm);
	
	public String getIssueType();
	public void  setIssueType (String IssueType );
	
	public String getSelectPriority();
	public void  setSelectPriority(String SelectPriority);
	
}