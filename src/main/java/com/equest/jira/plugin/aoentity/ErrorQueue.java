package com.equest.jira.plugin.aoentity;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;

 
@Preload
public interface ErrorQueue extends Entity
{
	public String getMessageId();
	public void setMessageId(String messageId);
	
	public String getEmailFrom();
	public void  setEmailFrom(String fromEmail);
	
	public String getSubject();
	public void  setSubject(String subject);
	
	public boolean getDelete();
	public void  setDelete(boolean option);
}
