package com.equest.jira.plugin.aoentity;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;

 
@Preload
public interface BlacklistedDomain extends Entity
{
	@AutoIncrement
	@NotNull
	@PrimaryKey("ID")
	public int getID();
	
	public String getUserEmail();
	public void setUserEmail(String userEmail);
	
	public String getDomain();
	public void  setDomain(String domain);
	
	public String getSubject();
	public void  setSubject(String subject);
	
}