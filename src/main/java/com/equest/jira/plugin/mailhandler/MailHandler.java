package com.equest.jira.plugin.mailhandler;


import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.service.util.handler.MessageHandler;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageHandlerErrorCollector;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.equest.jira.plugin.condition.Privilege;
import com.equest.jira.plugin.messagehandler.CommentHandler;
import com.equest.jira.plugin.messagehandler.CreateIssueHandler;
import com.equest.jira.plugin.messagehandler.FileAttachmentHandler;
import com.equest.jira.plugin.messagehandler.MessageProcessor;
import com.equest.jira.plugin.utility.AoUtility;
import com.equest.jira.plugin.utility.TextUtility;
import com.atlassian.jira.service.util.ServiceUtils;
import com.atlassian.mail.MailUtils;
import com.atlassian.activeobjects.external.ActiveObjects;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import java.util.Map;
import javax.mail.Message;
import javax.mail.MessagingException;
import com.equest.jira.plugin.utility.WorkflowUtility;
 
public class MailHandler implements MessageHandler {
	 	private String issueType;
	 	private String projectKey;
	    private final Validator validator;
	    private final MessageUserProcessor messageUserProcessor;
	    public static final String ISSUE_TYPE = "issueType";
	    public static final String PROJECT_KEY = "projectKey";
	    CommentHandler commentHandler = new CommentHandler();
	    ActiveObjects ao = null;
	    AoUtility newAoUtility = null;
	    TextUtility textUtility = new TextUtility();
	    WorkflowUtility workflowUtility = new WorkflowUtility();
	    MessageProcessor messageProcessor = new MessageProcessor();
	    
	    private static Logger log=Logger.getLogger(MessageHandler.class);
	 
	    // we can use dependency injection here too!
	    	public MailHandler(MessageUserProcessor messageUserProcessor, Validator validator, ActiveObjects ao) {
	        this.messageUserProcessor = messageUserProcessor;
	        this.validator = validator;
	        this.ao = ao;
	        newAoUtility = new AoUtility(this.ao);
	    }
	 
	    @Override
	    public void init(Map<String, String> params, MessageHandlerErrorCollector monitor) {
	        // getting param configured by the user
	        issueType = params.get(ISSUE_TYPE);
	        projectKey = params.get(PROJECT_KEY);
	        
	        if (StringUtils.isBlank(issueType)) {
	            // this message will be either logged or displayed to the user (if the handler is tested from web UI)
	            monitor.error("IssueType has not been specified ('" + ISSUE_TYPE + "' parameter). This handler will not work correctly.");
	        }
	        if (StringUtils.isBlank(projectKey)) {
	            // this message will be either logged or displayed to the user (if the handler is tested from web UI)
	            monitor.error("Project Key has not been specified ('" + PROJECT_KEY + "' parameter). This handler will not work correctly.");
	        }
	        validator.validateIssue(projectKey,issueType, monitor);
	    }
	 
	    @Override
	    public boolean handleMessage(Message message, MessageHandlerContext context) throws MessagingException {
	    	
	    	String messageSubject =  null;
	    	Boolean doDelete=false;
	    	String messageBody = null;
	    	messageSubject = message.getSubject();
	    	Issue issue = ServiceUtils.findIssueObjectInString(messageSubject);
	    	Privilege privilege = new Privilege(ao);
	    	messageBody = MailUtils.getBody(message);
        	messageSubject = message.getSubject();
        	String fromMail = textUtility.getOnlyEmailId(message.getFrom());
        	String messageId = textUtility.getMessageId(message.getSentDate(), fromMail);
   
        	if(privilege.checkUser(fromMail) || privilege.checkSubject(messageSubject))
        	{
        		System.out.println(" The User is Blocked : " +fromMail);
        		return true;
        	}
        	else
        	{
        //	if(privilege.checkSubject(messageSubject))
        //	{
        //		System.out.println(" checkSubject : True :  " +messageSubject);
        //	}
        	
        //	System.out.println("---Error Message Id : " +!newAoUtility.checkMessageId(messageId) +" Subject : " +messageSubject);
        	
        	if( !(messageBody ==null || (messageBody.equals(""))) && !((messageSubject == null) || messageSubject.equals("")) && !commentHandler.isAutoSubmitted(message))
        	{
        		if(!newAoUtility.checkMessageId(messageId))
        		{
        			 newAoUtility.addErrorQueue(messageId, fromMail, messageSubject);
			    	 if (issue == null)
			         {
			             // If we cannot find the issue from the subject of the e-mail message
			             // try finding the issue using the in-reply-to message id of the e-mail message
			             log.debug("Issue Key not found in subject '" + messageSubject + "'. Inspecting the in-reply-to message ID.");
			             issue = ComponentAccessor.getMailThreadManager().getAssociatedIssueObject(message);
			         }
		
			         // if we have found an associated issue
			         if (issue != null)
			         {
			        	 // Add the message as comment
			        	 if (log.isDebugEnabled())
			             {
			                 log.debug("Issue found for email '" + messageSubject + "' - Adding message as comment.");
			             }
			        	 doDelete = commentHandler.AddComment(message,context,messageUserProcessor,issue.getKey());
			        	 FileAttachmentHandler fileAttachmentHandler = new FileAttachmentHandler();
			        	 fileAttachmentHandler.addAttachementIfAny(message, issue,context);      	
			        	 commentHandler.updateMailTrail(issue, message);
			        	 
			        	 Map<String, Object> rawData = messageProcessor.segregateData(message);
			        	 if( rawData.containsKey("Status"))
			        		 workflowUtility.updateStatus(issue, rawData.get("Status").toString(), messageUserProcessor.getAuthorFromSender(message));
			         }
			         else
			         {
			        	 CreateIssueHandler createIssueHandler = new CreateIssueHandler();
			        	 //no issue found, so create new issue
			        	 if (log.isDebugEnabled())
			             {
			                 log.debug("No Issue found for email '" + messageSubject + "' - creating a new Issue.");
			             }
			        	 doDelete = createIssueHandler.createIssue(message, context, messageUserProcessor,projectKey,issueType);
			     
			         }	 
			         
			         if(doDelete)
		        	 {
		        		 newAoUtility.deleteErrorQueue(messageId);
		        		// System.out.println("--------Message Id Deleted --------------");
		        	 }
			         
		          } 
        	}
        	// else
        	// {
        	//	 	System.out.println("------------------------ Message Deleted -----------------" +messageSubject);
        	//	 	return true;
        	// }
        	}
	    	return doDelete;
	    }
	}