package com.equest.jira.plugin.mailhandler;

import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.plugins.mail.webwork.AbstractEditHandlerDetailsWebAction;
import com.atlassian.jira.service.JiraServiceContainer;
import com.atlassian.jira.service.services.file.AbstractMessageHandlingService;
import com.atlassian.jira.service.util.ServiceUtils;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableMap;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.plugins.mail.model.HandlerDetailsModel;
import com.atlassian.jira.plugins.mail.model.ProjectModel;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.atlassian.jira.plugins.mail.model.IssueTypeModel;
import com.atlassian.jira.plugins.mail.HandlerDetailsValidator;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.google.common.base.Function;

import javax.annotation.Nonnull;



import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
public class EditDemoHandlerDetailsWebAction extends AbstractEditHandlerDetailsWebAction {
    
	
	private static final long serialVersionUID = 1L;

	private final Validator validator;
	private final List<Project> projects;
	private final IssueTypeSchemeManager issueTypeSchemeManager;
	private String detailsJson;
	private HandlerDetailsModel details;
	private final HandlerDetailsValidator detailsValidator;
    
	private static final Map<String, String> fieldLabels = ImmutableMap.<String, String>builder()
            .put("project", "common.concepts.project")
            .put("issuetype", "common.concepts.issuetype")  
            .put("reporterusername", "jmp.editHandlerDetails.reporterusername")
            .put("splitregex", "jmp.editHandlerDetails.splitregex")
            .put("catchemail", "jmp.editHandlerDetails.catchemail")
            .put("ccassignee", "jmp.editHandlerDetails.ccassignee")
            .put("ccwatcher", "jmp.editHandlerDetails.ccwatcher")
            .put("port", "jmp.editHandlerDetails.port")
            .put("usessl", "jmp.editHandlerDetails.usessl")
            .build();
 
    public EditDemoHandlerDetailsWebAction(IssueTypeSchemeManager issueTypeSchemeManager, ProjectManager projectManager, PluginAccessor pluginAccessor,HandlerDetailsValidator detailsValidator, Validator validator) {
        super(pluginAccessor);
        this.validator = validator;
        this.projects = projectManager.getProjectObjects();
        this.issueTypeSchemeManager = issueTypeSchemeManager;  
        this.detailsValidator = detailsValidator;
    }
    private String issueType;
    private String projectKey;
    
   
/*    
    public String getIssueKey() {
        return issueKey;
    }
 
    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey;
    }
   */
    
    public String getIssueType() {
        return issueType;
    }
 
    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }
    
    public String getProjectKey() {
        return projectKey;
    }
 
    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }
 
    // this method is called to let us populate our variables (or action state)
    // with current handler settings managed by associated service (file or mail).
    @Override
    protected void copyServiceSettings(JiraServiceContainer jiraServiceContainer) throws ObjectConfigurationException {
        final String params = jiraServiceContainer.getProperty(AbstractMessageHandlingService.KEY_HANDLER_PARAMS);
        final Map<String, String> parameterMap = ServiceUtils.getParameterMap(params);
        issueType = parameterMap.get(MailHandler.ISSUE_TYPE);
        projectKey = parameterMap.get(MailHandler.PROJECT_KEY);
    }
 
    @Override
    protected Map<String, String> getHandlerParams() {
    	Map<String,String> handlerParams = new HashMap<String,String>();
    	handlerParams.put(MailHandler.PROJECT_KEY, projectKey);
    	handlerParams.put(MailHandler.ISSUE_TYPE, issueType);
    //	MapBuilder.build(MailHandler.PROJECT_KEY, issueType);
        return handlerParams;
    }
 
    @Override
    protected void doValidation() {
        if (configuration == null) {
            return; // short-circuit in case we lost session, goes directly to doExecute which redirects user
        }
        super.doValidation();
        validator.validateIssue(projectKey,issueType, new WebWorkErrorCollector());
        if (detailsJson == null) {
            addErrorMessage("No configuration data sent via detailsJson field.");
            return;
        }

        try {
			details = new ObjectMapper().readValue(detailsJson, HandlerDetailsModel.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

        addErrorCollection(detailsValidator.validateDetails(details));
    }
    
    @Nonnull
    public String getProjectsJson() {
        final List<ProjectModel> suggestions = Lists.newArrayList(
                Iterables.transform(projects, new Function<Project, ProjectModel>()
        {
            @Override
            public ProjectModel apply(final Project project)
            {
                return new ProjectModel(project.getName(), project.getKey(), Lists.<IssueTypeModel>newArrayList(
                    Iterables.transform(issueTypeSchemeManager.getIssueTypesForProject(project), new Function<IssueType, IssueTypeModel>()
                    {
                        @Override
                        public IssueTypeModel apply(IssueType from)
                        {
                            return new IssueTypeModel(from.getName(), from.getId());
                        }
                    })));
            }
        }));

        try {
			return new ObjectMapper().writeValueAsString(suggestions);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }
    
    public static Map<String, String> getFieldLabels() {
        return fieldLabels;
    }
    
    @SuppressWarnings("unused")
    public void setDetailsJson(String json) {
        this.detailsJson = json;
    }

    @Nonnull
    public String getDetailsJson() {
        try {
			return new ObjectMapper().writeValueAsString(details);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }
    
}