package com.equest.jira.plugin.mailhandler;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.service.util.handler.MessageHandlerErrorCollector;
import com.equest.jira.plugin.utility.ProjectUtil;

import org.apache.commons.lang.StringUtils;
 
public class Validator
{
    private final IssueManager issueManager;
 
    public Validator(IssueManager issueManager) {
        this.issueManager = issueManager;
    }
 
    public Project validateIssue(String projectKey,String issueType,  MessageHandlerErrorCollector collector) {
        if (StringUtils.isBlank(issueType)) {
            collector.error("Issue Type cannot be undefined.");
            return null;
        }
        
        if (StringUtils.isBlank(projectKey)) {
            collector.error("Project key cannot be undefined.");
            return null;
        }
        
        System.out.println("Project Key  : " + projectKey+" & Issue Type : "+issueType);
        ProjectManager projectManager = ComponentAccessor.getProjectManager();
        final Project projectObject = projectManager.getProjectObjByKey(projectKey);
 
   //     final Issue issue = issueManager.getIssueObject(issueKey);
        if (projectObject == null) {
            collector.error("The Project key does not exist.");
            return null;
        }
        else
        {
        	ProjectUtil projectUtil =new ProjectUtil();
        	Long id = projectUtil.getIssueTypeId(projectObject.getId(),issueType);
        	if(id==null)
        		return null;
        }
        
        return projectObject;
    }
}