package com.equest.jira.plugin.mailhandler;

import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.plugins.mail.webwork.AbstractEditHandlerDetailsWebAction;
import com.atlassian.jira.service.JiraServiceContainer;
import com.atlassian.jira.service.services.file.AbstractMessageHandlingService;
import com.atlassian.jira.service.util.ServiceUtils;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.PluginAccessor;
 
import java.util.HashMap;
import java.util.Map;
 
public class MailHandlerAction extends AbstractEditHandlerDetailsWebAction {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Validator validator;
	private String projectKey;
	private String issueType;
 
    public MailHandlerAction(PluginAccessor pluginAccessor, Validator validator) {
        super(pluginAccessor);
        this.validator = validator;
    }
    
    /*
    private String issueKey;
    public String getIssueKey() {
        return issueKey;
    }
 
    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey;
    }
    
    */
    
    public String getIssueType() {
        return issueType;
    }
 
    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }
    
    public String getProjectKey() {
        return projectKey;
    }
 
    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }
 
    // this method is called to let us populate our variables (or action state) 
    // with current handler settings managed by associated service (file or mail).
    @Override
    protected void copyServiceSettings(JiraServiceContainer jiraServiceContainer) throws ObjectConfigurationException {
        final String params = jiraServiceContainer.getProperty(AbstractMessageHandlingService.KEY_HANDLER_PARAMS);
        final Map<String, String> parameterMap = ServiceUtils.getParameterMap(params);
        issueType = parameterMap.get(MailHandler.ISSUE_TYPE);
        projectKey = parameterMap.get(MailHandler.PROJECT_KEY);  
        
    }
 
    @Override
    protected Map<String, String> getHandlerParams() {
    	Map<String,String> handlerParams = new HashMap<String,String>();
    	handlerParams.put(MailHandler.PROJECT_KEY, projectKey);
    	handlerParams.put(MailHandler.ISSUE_TYPE, issueType);
    //	MapBuilder.build(MailHandler.PROJECT_KEY, issueType);
        return handlerParams;
    }
 
    @Override
    protected void doValidation() {
        if (configuration == null) {
            return; // short-circuit in case we lost session, goes directly to doExecute which redirects user
        }
        super.doValidation();
        validator.validateIssue(projectKey,issueType, new WebWorkErrorCollector());
    }
}