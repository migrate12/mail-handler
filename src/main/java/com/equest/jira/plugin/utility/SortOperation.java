package com.equest.jira.plugin.utility;

import java.util.*;

public class SortOperation {

	    public static <K, V extends Comparable<? super V>> Map<K, V> byValueAsce( Map<K, V> map ){
	        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
	        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
	        {
	            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
	            {
	                return (o1.getValue()).compareTo( o2.getValue() );
	            }
	        } );

	        Map<K, V> result = new LinkedHashMap<K, V>();
	        for (Map.Entry<K, V> entry : list)
	        {
	            result.put( entry.getKey(), entry.getValue() );
	        }
	        return result;
	    }
	    
	    public static <K, V extends Comparable<? super V>> Map<K, V> byValueDesc( Map<K, V> map ){
	        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
	        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
	        {
	            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
	            {
	                return (o2.getValue()).compareTo( o1.getValue() );
	            }
	        } );

	        Map<K, V> result = new LinkedHashMap<K, V>();
	        for (Map.Entry<K, V> entry : list)
	        {
	            result.put( entry.getKey(), entry.getValue() );
	        }
	        return result;
	    }
	    
	    /*
	     * Paramterized method to sort Map e.g. HashMap in Java
	     * throw NullPointerException if Map contains null key
	     */
	    public static <K extends Comparable,V extends Comparable> Map<K,V> byKeyAsce(Map<K,V> map){
	        
	    	Map<K,V> treeMap = new TreeMap<K,V>(map);
	        return treeMap;
	    }
	    
	    public static <K extends Comparable,V extends Comparable> Map<K,V> byKeyDesc(Map<K,V> map){
	        
	    	Map<K,V> treeMap = new TreeMap<K,V>(Collections.reverseOrder());
	    	treeMap.putAll(map);
	    	
	        return treeMap;
	    }
	    
}
