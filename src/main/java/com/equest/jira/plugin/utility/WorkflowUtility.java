package com.equest.jira.plugin.utility;

import java.util.List;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.TransitionValidationResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.opensymphony.workflow.loader.ActionDescriptor;

public class WorkflowUtility {

	public void updateStatus(Issue issue, String status, User user)
	{
		JiraWorkflow workFlow = ComponentAccessor.getWorkflowManager().getWorkflow(issue);
		GenericValue currentStatus = issue.getStatusObject().getGenericValue();
		com.opensymphony.workflow.loader.StepDescriptor currentStep = workFlow.getLinkedStep(currentStatus);
		List<ActionDescriptor> actions = currentStep.getActions();
		
		for(ActionDescriptor action : actions)
		{
			System.out.println(" Status : " + action.getName() );
			//if(action.getName().equalsIgnoreCase("Close Issue"))
			if(action.getName().equalsIgnoreCase(status))	
			{
				System.out.println("Id of thransition " +action.getId());
				if(user !=  null)
				{
					System.out.println(" Issue status changed : " +transitionIssue(issue, action.getId(),user));
				}
			}
			
			
			
			//Label l = new Label(null, issue.getId(), "test");
			
		}
		
		
		
	}
		
		private boolean transitionIssue(Issue issue, int actionId,User user) {
			 
		    boolean result = false;
		    IssueService issueService = ComponentAccessor.getIssueService();
		    IssueService.IssueResult transResult;	     
		    IssueInputParameters issueInputParameters = new IssueInputParametersImpl();
		     
		    TransitionValidationResult validationResult = issueService.validateTransition(user, issue.getId(), actionId, issueInputParameters);
		    result = validationResult.isValid();
		    if (result) {
		        transResult = issueService.transition(user, validationResult);
		    }
		    return result;
		}
	
		
		
	
}
