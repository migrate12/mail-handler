package com.equest.jira.plugin.utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;

public class ProjectUtil {
	
	public Map<String,String> getPriority()
    {
		Map<String, String>  priority = new HashMap<String, String>();
    	ConstantsManager getConstantsManager = ComponentAccessor.getConstantsManager();
		Collection<Priority> priorityObject = getConstantsManager.getPriorityObjects();
		Iterator<Priority> priorityIterator =  priorityObject.iterator();
	//	System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Priority List !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		while(priorityIterator.hasNext())
		{
			Priority tempPriority = (Priority)priorityIterator.next();
		//	System.out.println(" Priority Name : " +tempPriority.getName());
		//	System.out.println(" Priority Name : " +tempPriority.getId());
			priority.put(tempPriority.getId(), tempPriority.getName());
		}
    	return com.equest.jira.plugin.utility.SortOperation.byValueAsce(priority);
    }
	
	public Map<String,String> getIssueType(Long proejectId)
	{
		Map<String,String>   issueTypes = new HashMap<String,String>();
		ProjectManager projectManager = ComponentAccessor.getProjectManager();
    	Project projectObject = projectManager.getProjectObj(proejectId);
    	ArrayList<IssueType> issuetypes = (ArrayList<IssueType>) projectObject.getIssueTypes();
    	Iterator<IssueType> issueTypeIterator = issuetypes.iterator();
    	while(issueTypeIterator.hasNext())
    	{
    		IssueType temp = issueTypeIterator.next();
    		issueTypes.put(temp.getId(), temp.getName());
    	}
    	return com.equest.jira.plugin.utility.SortOperation.byValueAsce(issueTypes);
	}
	
	public Long getIssueTypeId(Long proejectId,String issueType)
	{
		Long issueTypeId =null;
		ProjectManager projectManager = ComponentAccessor.getProjectManager();
    	Project projectObject = projectManager.getProjectObj(proejectId);
    	ArrayList<IssueType> issuetypes = (ArrayList<IssueType>) projectObject.getIssueTypes();
    	Iterator<IssueType> issueTypeIterator = issuetypes.iterator();
    	while(issueTypeIterator.hasNext())
    	{
    		IssueType temp = issueTypeIterator.next();
    		if((temp.getName()).equals(issueType))
    			issueTypeId = new Long(temp.getId());
    	}
    	return issueTypeId;
	}
	
	public Map<Long,String> getAllProject()
	{
		Map<Long,String>   projects = new HashMap<Long,String>();
		ProjectManager projectManager = ComponentAccessor.getProjectManager();
		List<Project> projectList = projectManager.getProjectObjects();
		Iterator<Project> projectsIterator = projectList.iterator();
		while(projectsIterator.hasNext())
		{
			Project temp= projectsIterator.next();	
			//System.out.println("Custome Field Name : " +temp.getName());
			//System.out.println("Custome Field Type: " +temp.getLeadUserName());
			projects.put(temp.getId() ,temp.getName());
		}
		return com.equest.jira.plugin.utility.SortOperation.byValueAsce(projects);
	}
	
	 /**
     * Returns a default system priority. If default system priority if not
     * set, tries to find 'middle' priority based on other priorities. It may
     * throw RuntimeException if there is not default priority set and there
     * are no other priorities (which is highly unlikely).
     *
     * @return a default system priority
     * @throws RuntimeException if no default set and no other priorities found.
     */
    public String getDefaultSystemPriority()
    {
    	ConstantsManager getConstantsManager = ComponentAccessor.getConstantsManager();
        // if priority header is not set, assume it's 'default'
        Priority defaultPriority = getConstantsManager.getDefaultPriorityObject();
        if (defaultPriority == null)
        {
        //    log.warn("Default priority was null. Using the 'middle' priority.");
            Collection<Priority> priorities = getConstantsManager.getPriorityObjects();
            final int times = (int) Math.ceil((double) priorities.size() / 2d);
            Iterator<Priority> priorityIt = priorities.iterator();
            for (int i = 0; i < times; i++)
            {
                defaultPriority = priorityIt.next();
            }
        }
        if (defaultPriority == null)
        {
            throw new RuntimeException("Default priority not found");
        }
        return defaultPriority.getId();
    }
	
	

	
	
	

}
