package com.equest.jira.plugin.utility;

import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.equest.jira.plugin.aoentity.BlacklistedDomain;
import com.equest.jira.plugin.aoentity.ErrorQueue;
import com.equest.jira.plugin.aoentity.Rules;
import com.equest.jira.plugin.aoentity.WhiteListedDomain;
import com.atlassian.sal.api.transaction.TransactionCallback;

public class AoUtility {
	
	private ActiveObjects ao = null;

	public AoUtility(ActiveObjects ao)
	{
		this.ao =ao;
	}
	
	public void addBlackListedDomain(final String userEmailId,final String domain,final String subject)
	{
		//System.out.println("== addding BlackListed Domian ==");
		ao.executeInTransaction(new TransactionCallback<BlacklistedDomain>() {
			@Override
			public BlacklistedDomain doInTransaction() {
				final BlacklistedDomain newBlacklistedDomain = ao.create(BlacklistedDomain.class);
				newBlacklistedDomain.setUserEmail(userEmailId);
				newBlacklistedDomain.setDomain(domain);
				newBlacklistedDomain.setSubject(subject);
				newBlacklistedDomain.save();
				return newBlacklistedDomain;
			}
		});
	}
	
	public BlacklistedDomain[] getAllBlackListedDomain()
	{	
		BlacklistedDomain[] temp = ao.find(BlacklistedDomain.class);
		return temp;
	}
	
	
	public void updateBlacklistedDomian(final String userEmailId,final String domain,final String subject,final int id)
	{
		final BlacklistedDomain[] upadatedomain = ao.find(BlacklistedDomain.class,Query.select().where("ID = ?", id));
		for(BlacklistedDomain blacklistedDomain : upadatedomain)
		{
				
			blacklistedDomain.setDomain(domain);			
			blacklistedDomain.setUserEmail(userEmailId);
			blacklistedDomain.setSubject(subject);	
			blacklistedDomain.save();		
		}		
	}
	
	
	public void deleteBlacklistedDomian(final int id)
	{
		final BlacklistedDomain[] blacklistedDomain = ao.find(BlacklistedDomain.class,Query.select().where("ID = ?", id));
		ao.delete(blacklistedDomain);
	}
	
	public void addWhiteListedDomain(final String userEmailId,final String domain,final String subject)
	{
		//System.out.println("== addding new WhiteListedDomain Domian ==");
		ao.executeInTransaction(new TransactionCallback<WhiteListedDomain>() {
			@Override
			public WhiteListedDomain doInTransaction() {
				final WhiteListedDomain newWhiteListedDomain = ao.create(WhiteListedDomain.class);
				newWhiteListedDomain.setUserEmail(userEmailId);
				newWhiteListedDomain.setDomain(domain);
				newWhiteListedDomain.setSubject(subject);
				newWhiteListedDomain.save();
				return newWhiteListedDomain;
			}
		});
	}
	
	public void updateWhiteListedDomain(final String userEmailId,final String domain,final String subject,final int id)
	{
		final WhiteListedDomain[] upadatedomain = ao.find(WhiteListedDomain.class,Query.select().where("ID = ?", id));
		for(WhiteListedDomain whiteListedDomain : upadatedomain)
		{
				
			whiteListedDomain.setDomain(domain);			
			whiteListedDomain.setUserEmail(userEmailId);
			whiteListedDomain.setSubject(subject);	
			whiteListedDomain.save();		
		}		
	}
	
	public WhiteListedDomain[] getAllWhiteListedDomain()
	{	
		WhiteListedDomain[] temp = ao.find(WhiteListedDomain.class);
		return temp;
	}
	
	public void deleteWhiteListedDomain(final int id)
	{
		final WhiteListedDomain[] whiteListedDomain = ao.find(WhiteListedDomain.class,Query.select().where("ID = ?", id));
		ao.delete(whiteListedDomain);
	}
	
	public void addRules(final String userEmailId,final String subjectContain,final String mailForm,final String issueType,final String selectPriority)
	{
		//	System.out.println("== addding new WhiteListedDomain Domian ==");
			ao.executeInTransaction(new TransactionCallback<Rules>() {
					@Override
					public Rules doInTransaction()
					{
							final Rules newRules = ao.create(Rules.class);
							newRules.setUserEmail(userEmailId);
							newRules.setSubjectContain(subjectContain);
							newRules.setMailForm(mailForm);
							newRules.setIssueType (issueType );
							newRules.setSelectPriority(selectPriority);
							newRules.save();
							return newRules;				
					}
			});
	}
	
	public Rules[] getAllRules()
	{	
		Rules[] temp = ao.find(Rules.class);
		return temp;
	}
	
	public void deleteRules(final int id)
	{
		final Rules[] whiteListedDomain = ao.find(Rules.class,Query.select().where("ID = ?", id));
		ao.delete(whiteListedDomain);
	}
	
	
	public void addErrorQueue(final String messageId,final String emailFrom,final String subject)
	{
		//System.out.println("== addding new ErrorQueue ==");
		ao.executeInTransaction(new TransactionCallback<ErrorQueue>() {
			@Override
			public ErrorQueue doInTransaction() {
				final ErrorQueue newErrorQueue = ao.create(ErrorQueue.class);
				newErrorQueue.setMessageId(messageId);
				newErrorQueue.setEmailFrom(emailFrom);
				newErrorQueue.setSubject(subject);
				newErrorQueue.setDelete(false);
				newErrorQueue.save();
				return newErrorQueue;
			}
		});
	}
	
	public void updateErrorQueue(final String messageId,final boolean value)
	{
		final ErrorQueue[] errorQueue = ao.find(ErrorQueue.class,Query.select().where("MESSAGE_ID = ?", messageId));
		for(ErrorQueue updateErrorQueue : errorQueue)
		{
			updateErrorQueue.setDelete(value);				
			updateErrorQueue.save();		
		}		
	}
	
	public ErrorQueue[] getAllErrorQueue()
	{	
		ErrorQueue[] temp = ao.find(ErrorQueue.class);
		return temp;
	}
	
	public void deleteErrorQueue(final String messageId)
	{
		final ErrorQueue[] errorQueue = ao.find(ErrorQueue.class,Query.select().where("MESSAGE_ID = ?", messageId));
		ao.delete(errorQueue);
	}
	
	public boolean checkMessageId(String messageId)
	{
		final ErrorQueue[] checkMessageId = ao.find(ErrorQueue.class,Query.select().where("MESSAGE_ID = ?", messageId));
		for(ErrorQueue errorQueue : checkMessageId)
		{
			if(errorQueue.getMessageId().equalsIgnoreCase(messageId))
				return true;
		}
		return false;
	}
	
	public boolean checkMailForDeletion(String messageId)
	{
		final ErrorQueue[] checkMessageId = ao.find(ErrorQueue.class,Query.select().where("MESSAGE_ID = ?", messageId));
		for(ErrorQueue errorQueue : checkMessageId)
		{
			if(errorQueue.getMessageId().equalsIgnoreCase(messageId) && errorQueue.getDelete())
				return true;
		}
		return false;
	}
	
	
	

}
