package com.equest.jira.plugin.utility;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.Address;

public class TextUtility {

	public static String cleanInvalidCharacters(String in) 
	{    
	    return checkString(in);
	}
	
	public String getOnlyEmailId(Address[] address)
	{
		String emailId = "";
		
		int count =0;
		 for(Address i : address)
		  { 
			if(count == 0)
			try 
			 {
				emailId = new InternetAddress(i.toString()).getAddress();
				if(count > 0)
					emailId = emailId + ","+ new InternetAddress(i.toString()).getAddress();
				count++;
			} 
			catch (AddressException e) 
			{
				e.printStackTrace();
			}
		 }
		 
		return emailId;
	}
	 
	 public String getMessageId(Date date,String mailFrom)
	 {
		 String messageId ="";
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		 messageId = dateFormat.format(date) + "_" +mailFrom  ;
		 return messageId;
	 }
	 
	 
	 public static String checkString(String temp)
	 {
		 //System.out.println("Value : " +temp);
		 String utf8tweet = temp;	    
		 try {
	         byte[] utf8Bytes = utf8tweet.getBytes("UTF-8");

	         utf8tweet = new String(utf8Bytes, "UTF-8");

	     } catch (UnsupportedEncodingException e) {
	         e.printStackTrace();
	     }
	     Pattern unicodeOutliers = Pattern.compile("[^\\x00-\\x7F]",
	             Pattern.UNICODE_CASE | Pattern.CANON_EQ
	                     | Pattern.CASE_INSENSITIVE);
	     Matcher unicodeOutlierMatcher = unicodeOutliers.matcher(utf8tweet);
	    // System.out.println("Before: " + utf8tweet);
	     utf8tweet = unicodeOutlierMatcher.replaceAll("?");
	   //  System.out.println("After: " + utf8tweet);
	     return utf8tweet; 
	 }
	 	
}
