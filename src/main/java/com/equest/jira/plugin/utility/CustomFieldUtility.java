package com.equest.jira.plugin.utility;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.Address;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;

public class CustomFieldUtility {

	public String getCustomFieldType(CustomField customField)
	{
		String[] type = (customField.getCustomFieldType().toString()).split("\\.");
	//	System.out.println(" Type : " +type.length);
		String[] ttype = type[type.length-1].split("@");
		String customefieldType = ttype[0];
	//	System.out.println("customefieldType : " +customefieldType);
		return customefieldType;
	}
	
	public Option getOption(CustomField customField,String value)
	{		
		Option option = null;
		Options options = ComponentAccessor.getOptionsManager().getOptions(customField.getConfigurationSchemes().listIterator().next().getOneAndOnlyConfig());
		Iterator<Option> optionIterator = options.iterator();
		while (optionIterator.hasNext())
            {
			    option = (Option)optionIterator.next();
			    if(value.equalsIgnoreCase(option.getValue()))
			    {
			    	return option;
			    }
            }
		return option;		
	}
	
	public Map<String,String> getCustomFields(Long proejectId)
	{
		Map<String,String> custom = new HashMap<String,String>();
		
		MutableIssue issueObject = ComponentAccessor.getIssueFactory().getIssue();
        issueObject.setProjectObject(ComponentAccessor.getProjectManager().getProjectObj(proejectId));
        issueObject.setIssueTypeId("1");
        issueObject.setSummary("summary");
        User reporter = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        issueObject.setReporter(reporter);
		List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
		for (CustomField customField : customFieldObjects)
	     {
	    	//System.out.println(" Name : " +customField.getName() +" Id : " +customField.getId());
	    	custom.put(customField.getId(), customField.getName());
	     }
	    
	    return custom;
	}
	
	public Map<Long,String> getCustomFieldOptions(CustomField customField)
	{
		Map<Long,String> customFieldOptions = new HashMap<Long,String>();
		
		Options options = ComponentAccessor.getOptionsManager().getOptions(customField.getConfigurationSchemes().listIterator().next().getOneAndOnlyConfig());
		Iterator<Option> optionIterator = options.iterator();
		
		   while (optionIterator.hasNext())
            {
			    Option option = (Option)optionIterator.next();
			    customFieldOptions.put(option.getOptionId(), option.getValue());
			  //  System.out.println("Custome Option Name : " +option.getValue() +" : Custome Option Id :  " +option.getOptionId());
            }
		   
		return com.equest.jira.plugin.utility.SortOperation.byValueAsce(customFieldOptions);
		
	}
	
	 public void saveValue(MutableIssue issue, Option valueToSave, CustomField
	            customField) throws FieldLayoutStorageException {

	        issue.setCustomFieldValue(customField, valueToSave);

	        Map<String, ModifiedValue> modifiedFields = issue.getModifiedFields();

	        FieldLayoutItem fieldLayoutItem = ComponentAccessor.getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(customField);
	    //    ComponentManager.getInstance().getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(customField);

	        DefaultIssueChangeHolder issueChangeHolder = new DefaultIssueChangeHolder();

	        final ModifiedValue modifiedValue = (ModifiedValue) modifiedFields.get(customField.getId());

	        customField.updateValue(fieldLayoutItem, issue, modifiedValue, issueChangeHolder);
	    }
	 
	 public void saveTextValue(MutableIssue issue, CustomField customField, String newValue)
	 {
		 	//System.out.println("====newValue===="+ newValue );
		 	//System.out.println("====customField===="+ customField.getName() );
		 	//System.out.println("====customField===="+ issue.getKey());
		 try{
	        issue.setCustomFieldValue(customField, newValue);
	        Map<String, ModifiedValue> modifiedFields = issue.getModifiedFields();
	        FieldLayoutItem fieldLayoutItem = ComponentAccessor.getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(customField);
	        //ComponentManager.getInstance().getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(customField);
	        DefaultIssueChangeHolder issueChangeHolder = new DefaultIssueChangeHolder();
	        final ModifiedValue modifiedValue = (ModifiedValue) modifiedFields.get(customField.getId());
	        customField.updateValue(fieldLayoutItem, issue, modifiedValue, issueChangeHolder);
	    } catch(NullPointerException ex){}
	 }
	 
}
