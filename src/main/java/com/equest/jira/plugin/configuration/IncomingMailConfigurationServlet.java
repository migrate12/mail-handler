package com.equest.jira.plugin.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.equest.jira.plugin.utility.AoUtility;
import com.equest.jira.plugin.utility.CustomFieldUtility;
import com.equest.jira.plugin.utility.ProjectUtil;


  public class IncomingMailConfigurationServlet extends HttpServlet
  {
	private TemplateRenderer renderer;
	private  ActiveObjects ao;
	
	private static final long serialVersionUID = 1L;
	Map<String, Object> param = new HashMap<String, Object>();
	Map<String, String>  priority = new HashMap<String, String>();
	Map<String, Map<Long,String>> customFields = new HashMap<String, Map<Long,String>>();
	ProjectUtil projectUtil = new ProjectUtil();
	CustomFieldUtility customFieldUtility =new CustomFieldUtility();
	AoUtility aoUtility = null;
	String baseUrl = ComponentAccessor.getApplicationProperties().getString("jira.baseurl");
	
	 public IncomingMailConfigurationServlet(TemplateRenderer templateRenderer, ActiveObjects ao)
	   {
		 this.renderer = templateRenderer;
		 this.ao = ao;
		 aoUtility =  new AoUtility(ao);
	   }
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    {  
	    	String submitValue = request.getParameter("submit");
	    	if(submitValue != null)
	    	{
		    	if(submitValue.equalsIgnoreCase("Add"))
		    	{
		    		doAdd(request,response);
		    	}
		    	
		    	if(submitValue.equalsIgnoreCase("Update"))
		    	{
		    		doUpdate(request,response);
		    	}
		    	
		    	if(submitValue.equalsIgnoreCase("Delete"))
		    	{
		    		dodelete(request,response);
		    	}
	    	}	
	    }
      
    
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    { 
    	
    	JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
  		if(authenticationContext.isLoggedInUser())
  		{
  		    GroupManager groupMngr = ComponentAccessor.getGroupManager();
  	        String username = authenticationContext.getUser().getName();
  	        if(!groupMngr.isUserInGroup(username, "jira-administrators"))
  	        {
  	        	response.sendError(550, "Permission Denied");
  	        }   	        
  	        else
  	        {
		    	param.put("baseUrl",ComponentAccessor.getApplicationProperties().getString("jira.baseurl"));
		    	param.put("AllBlackListedDomain", aoUtility.getAllBlackListedDomain());
		    	param.put("AllWhiteListedDomain", aoUtility.getAllWhiteListedDomain());
		    	this.renderer.render("/templates/selectProject.vm", param, response.getWriter());
  	        }
  		}  
   }
    
      
    protected void doAdd(HttpServletRequest request, HttpServletResponse response)
  	      throws ServletException, IOException
  { 
  		
  	String userEmailId = request.getParameter("userEmailId");
  	String domain = request.getParameter("domainId");
  	String subjectContain = request.getParameter("subjectContain");
  	String operation = request.getParameter("operation");
  	
  	if(operation.equals("BlackListed"))
  		aoUtility.addBlackListedDomain(userEmailId, domain, subjectContain);
  	else
  		aoUtility.addWhiteListedDomain(userEmailId, domain, subjectContain);
  	
  	response.sendRedirect(baseUrl +"/plugins/servlet/IncomingMailConfigurationServlet");
  	
  } 
    
    protected void doUpdate(HttpServletRequest request, HttpServletResponse response)
    	      throws ServletException, IOException
    { 
    		
    	String userEmailId = request.getParameter("userEmailId");
    	String domain = request.getParameter("domainId");
    	String subjectContain = request.getParameter("subjectContain");
    	String Id = request.getParameter("Id");
    	String operation = request.getParameter("operation");
    	
    	if(operation.equals("BlackListed"))
    		aoUtility.updateBlacklistedDomian(userEmailId, domain, subjectContain, new Integer(Id));
    	else
    		aoUtility.updateWhiteListedDomain(userEmailId, domain, subjectContain,new Integer(Id));
    	
    	response.sendRedirect(baseUrl +"/plugins/servlet/IncomingMailConfigurationServlet");
    	
    	
    } 
    
    protected void dodelete(HttpServletRequest request, HttpServletResponse response)
  	      throws ServletException, IOException
	  { 
	  
	    	String Id = request.getParameter("Id");
	    	String operation = request.getParameter("operation");	
	    	if(operation.equals("BlackListed"))
	    		aoUtility.deleteBlacklistedDomian(new Integer(Id));
	    	else
	    		aoUtility.deleteWhiteListedDomain(new Integer(Id));
	    	
	    	response.sendRedirect(baseUrl +"/plugins/servlet/IncomingMailConfigurationServlet");
	  }   
    
    
  }

