package com.equest.jira.plugin.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.templaterenderer.TemplateRenderer;

  public class UserUtilityServerlet extends HttpServlet
  {
	private TemplateRenderer renderer;
	private static final long serialVersionUID = 1L;
	Map<String,Object> param = new HashMap<String, Object>();
	
	
	ProjectManager projectManager = null;
	String project = null;
	Map<String,String> issueTypes = new HashMap<String,String>();
	
	
	
	
	 public UserUtilityServerlet(TemplateRenderer templateRenderer)
	   {
		 this.renderer = templateRenderer;
	   }
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    {  
    	Long proejectId = new Long(request.getParameter("projectId"));
    	System.out.println("Project Id : " +request.getParameter("projectId"));
    	
    	projectManager = ComponentAccessor.getProjectManager();
    	project = projectManager.getProjectObjByKey("DEMO").getName();
    
    	
    	Project projectObject = projectManager.getProjectObj(proejectId);
    	
    	 
    	ArrayList<IssueType> issuetypes = (ArrayList<IssueType>) projectObject.getIssueTypes();
    	Iterator<IssueType> issueTypeIterator = issuetypes.iterator();
    	while(issueTypeIterator.hasNext())
    	{
    		IssueType temp = issueTypeIterator.next();
    		issueTypes.put(temp.getId(), temp.getName());
    	}
    
    	param.put("baseUrl",ComponentAccessor.getApplicationProperties().getString("jira.baseurl"));
    	param.put("projectKey",projectObject.getKey());
    	param.put("issueTypes", issueTypes);
    	this.renderer.render("/templates/addUser.vm", param, response.getWriter());
     }
      
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    { 
    	doPost(request,response);
    }    
    
  }

