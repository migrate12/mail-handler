package com.equest.jira.plugin.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;

public class ProjectUtil {
	
	protected Map<String,String> getPriority()
    {
		Map<String, String>  priority = new HashMap<String, String>();
    	ConstantsManager getConstantsManager = ComponentAccessor.getConstantsManager();
		Collection<Priority> priorityObject = getConstantsManager.getPriorityObjects();
		Iterator<Priority> priorityIterator =  priorityObject.iterator();
	//	System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Priority List !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		while(priorityIterator.hasNext())
		{
			Priority tempPriority = (Priority)priorityIterator.next();
		//	System.out.println(" Priority Name : " +tempPriority.getName());
		//	System.out.println(" Priority Name : " +tempPriority.getId());
			priority.put(tempPriority.getId(), tempPriority.getName());
		}
    	return priority;
    }
	
	protected Map<String,String> getIssueType(Long proejectId)
	{
		Map<String,String>   issueTypes = new HashMap<String,String>();
		ProjectManager projectManager = ComponentAccessor.getProjectManager();
    	Project projectObject = projectManager.getProjectObj(proejectId);
    	ArrayList<IssueType> issuetypes = (ArrayList<IssueType>) projectObject.getIssueTypes();
    	Iterator<IssueType> issueTypeIterator = issuetypes.iterator();
    	while(issueTypeIterator.hasNext())
    	{
    		IssueType temp = issueTypeIterator.next();
    		issueTypes.put(temp.getId(), temp.getName());
    	}
    	return issueTypes;
	}
	
	protected Map<Long,String> getAllProject()
	{
		Map<Long,String>   projects = new HashMap<Long,String>();
		ProjectManager projectManager = ComponentAccessor.getProjectManager();
		List<Project> projectList = projectManager.getProjectObjects();
		Iterator projectsIterator = projectList.iterator();
		while(projectsIterator.hasNext())
		{
			Project temp= (Project)projectsIterator.next();	
			//System.out.println("Custome Field Name : " +temp.getName());
			//System.out.println("Custome Field Type: " +temp.getLeadUserName());
			projects.put(temp.getId() ,temp.getName());
		}
		return projects;
	}
	
	protected Map<Long,String> getCustomFieldOptions(CustomField customField)
	{
		Map<Long,String> customFieldOptions = new HashMap<Long,String>();
		
		Options options = ComponentAccessor.getOptionsManager().getOptions(customField.getConfigurationSchemes().listIterator().next().getOneAndOnlyConfig());
		Iterator<Option> optionIterator = options.iterator();
		
		   while (optionIterator.hasNext())
            {
			    Option option = (Option)optionIterator.next();
			    customFieldOptions.put(option.getOptionId(), option.getValue());
			    System.out.println("Custome Option Name : " +option.getValue() +"Custome Option Id :  " +option.getOptionId());
            }
		   
		return customFieldOptions;
		
	}
	
	public Map<String,String> getCustomFields(Long proejectId)
	{
		Map<String,String> custom = new HashMap<String,String>();
		
		MutableIssue issueObject = ComponentAccessor.getIssueFactory().getIssue();
        issueObject.setProjectObject(ComponentAccessor.getProjectManager().getProjectObj(proejectId));
        issueObject.setIssueTypeId("1");
        issueObject.setSummary("summary");
        User reporter = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        issueObject.setReporter(reporter);
		List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
	    System.out.println("-------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------");
		for (CustomField customField : customFieldObjects)
	     {
	    	System.out.println(" Name : " +customField.getName() +" Id : " +customField.getId());
	    	custom.put(customField.getId(), customField.getName());
	     }
	    
	    return custom;
	}
	

}
