package com.equest.jira.plugin.configuration;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.equest.jira.plugin.utility.AoUtility;
import com.equest.jira.plugin.utility.CustomFieldUtility;
import com.equest.jira.plugin.utility.ProjectUtil;

public class ErrorQueueServlet  extends HttpServlet
{	
	private TemplateRenderer renderer;
	
	private static final long serialVersionUID = 1L;
	Map<String, Object> param = new HashMap<String, Object>();
	Map<String, String>  priority = new HashMap<String, String>();
	Map<String, Map<Long,String>> customFields = new HashMap<String, Map<Long,String>>();
	ProjectUtil projectUtil = new ProjectUtil();
	CustomFieldUtility customFieldUtility =new CustomFieldUtility();
	AoUtility aoUtility = null;
	String baseUrl = ComponentAccessor.getApplicationProperties().getString("jira.baseurl");
	
	 public ErrorQueueServlet(TemplateRenderer templateRenderer, ActiveObjects ao)
	   {
		 this.renderer = templateRenderer;
		 aoUtility =  new AoUtility(ao);
	   }
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    {  
	    	String submitValue = request.getParameter("submit");
	    	if(submitValue != null)
	    	{
		    	if(submitValue.equalsIgnoreCase("Delete"))
		    	{
		    		dodelete(request,response);
		    	}
	    	}	
	    }
      
    
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    { 
    	
    	JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
  		if(authenticationContext.isLoggedInUser())
  		{
  		    GroupManager groupMngr = ComponentAccessor.getGroupManager();
  	        String username = authenticationContext.getUser().getName();
  	        if(!groupMngr.isUserInGroup(username, "jira-administrators"))
  	        {
  	        	response.sendError(550, "Permission Denied");
  	        }   	        
  	        else
  	        {
		    	param.put("baseUrl",ComponentAccessor.getApplicationProperties().getString("jira.baseurl"));
		    	param.put("ErrorQueue", aoUtility.getAllErrorQueue());
		    	this.renderer.render("/templates/ErrorQueue.vm", param, response.getWriter());
  	        }
  		}  
   }
   
    protected void dodelete(HttpServletRequest request, HttpServletResponse response)
  	      throws ServletException, IOException
	  { 
	    	String Id = request.getParameter("messageId");
	    	aoUtility.updateErrorQueue(Id,true);
	    	response.sendRedirect(baseUrl +"/plugins/servlet/ErrorQueueServlet");
	  }   	
	    
}


