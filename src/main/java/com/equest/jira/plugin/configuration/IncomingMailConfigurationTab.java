package com.equest.jira.plugin.configuration;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;








import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;


public class IncomingMailConfigurationTab extends AbstractProjectTabPanel{
	
	Map<String, Object> parms = new HashMap<String, Object>();
	
	public boolean showPanel(BrowseContext arg0) 
	{
		return true;
	}

		
	public String getHtml(BrowseContext browseContext ) {
		
		ArrayList<IssueType> issueTypes = (ArrayList<IssueType>) browseContext.getProject().getIssueTypes();
		
		Iterator issuetypeIterator =issueTypes.iterator() ;
		while (issuetypeIterator.hasNext())
		{
			
			IssueType temp = (IssueType) issuetypeIterator.next();
			System.out.println(" Issue Id :  " +temp.getId() +" Issue Type Name : " +temp.getName());
		
		}
	
		parms.put("baseUrl",ComponentAccessor.getApplicationProperties().getString("jira.baseurl"));
		parms.put("projectKey", browseContext.getProject().getKey());
		parms.put("projectId", browseContext.getProject().getId());
		
		return descriptor.getHtml("view", parms);
	}

}