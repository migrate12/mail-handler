package com.equest.jira.plugin.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.atlassian.templaterenderer.TemplateRenderer;

  public class DomainUtilityServerlet extends HttpServlet
  {
	private TemplateRenderer renderer;
	private static final long serialVersionUID = 1L;
	Map<String,Object> param = new HashMap<String, Object>();
	
	
	
	 public DomainUtilityServerlet(TemplateRenderer templateRenderer)
	   {
		 this.renderer = templateRenderer;
	   }
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    {  
    		this.renderer.render("/templates/IncomingMailConfigurationServlet.vm", param, response.getWriter());
     }
      
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
    { 
    	doPost(request,response);
    }    
    
  }

