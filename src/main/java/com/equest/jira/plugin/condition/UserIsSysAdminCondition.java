package com.equest.jira.plugin.condition;

import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

public class UserIsSysAdminCondition implements Condition {
	 
    @Override
   public void init(Map<String, String> params) throws PluginParseException {
      
   }

      @Override
   public boolean shouldDisplay(Map<String, Object> context) {
       GroupManager groupMngr = ComponentAccessor.getGroupManager();
       JiraAuthenticationContext authCtx =  ComponentAccessor.getJiraAuthenticationContext();
             if (authCtx.isLoggedInUser()) {
           String username = authCtx.getUser().getName();
           return groupMngr.isUserInGroup(username, "jira-administrators");
       }
        
       return false;
   }
}
