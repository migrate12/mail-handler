package com.equest.jira.plugin.condition;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.equest.jira.plugin.aoentity.BlacklistedDomain;
import com.equest.jira.plugin.aoentity.WhiteListedDomain;

import net.java.ao.Query;

public class Privilege {
	
	private ActiveObjects ao = null;
	
	public Privilege(ActiveObjects ao)
	{
		this.ao =ao;
	}

	public Boolean checkUser(String userEmailId)
	{
		
		String[] temp = userEmailId.split("@");
		System.out.println("");
		
		final BlacklistedDomain[] checkEmailId = ao.find(BlacklistedDomain.class,Query.select().where("USER_EMAIL = ?", userEmailId));
		 for(BlacklistedDomain domian : checkEmailId)
		{
			// System.out.println(" Testing Email Id : " +domian.getUserEmail());
			// System.out.println(" Testing Domain  : " +domian.getDomain());
			// System.out.println("----------------Testing the Email Id -----------------");
			 if((domian.getUserEmail()).equalsIgnoreCase(userEmailId))
	        		return true;			 
		}
		
        final BlacklistedDomain[] chackDomain = ao.find(BlacklistedDomain.class,Query.select().where("DOMAIN = ?", temp[1]));
        for(BlacklistedDomain domian : chackDomain)
		{
        	//System.out.println(" Testing Email Id : " +domian.getUserEmail());
        	//System.out.println(" Testing Domain  : " +domian.getDomain());
        	//System.out.println("----------------Testing the Domain Id -----------------");
        	
        	final WhiteListedDomain[] whiteListedDomain = ao.find(WhiteListedDomain.class,Query.select().where("USER_EMAIL = ?", userEmailId));
        	for(WhiteListedDomain whiteDomian : whiteListedDomain)
        	 {
        			if((whiteDomian.getUserEmail()).equalsIgnoreCase(userEmailId))
        			{
        				//System.out.println("----User is in White Listed List---");
        				return false;
        			}
        	}
        	return true;        			
        } 			
		
       // System.out.println(" ------ User not in Both List ---------");
		return false;
	}
	
	public Boolean checkRule(String userEmail, String subjectContain,String mailId, Message message)
	{
		String emailId="";
		String subject = ""; 
		Address[] t;
		try {
			
					subject = message.getSubject();
					t = message.getFrom();
					
					   for(Address i : t)
					   {
						   emailId = ((InternetAddress) i).getAddress();
					   }
					   
					if(userEmail.equals(emailId))
					{
						return true;
					}
					if(subject.contains(subjectContain))
					{
						return true;
					}
					if(mailId.contains(emailId))
					{
						return true;
					}	
			} 
		catch (MessagingException e) {
			e.printStackTrace();
		}
	
		return false;
	}	
	
	public Boolean checkSubject(String subject)
	{		
		final BlacklistedDomain[] checkEmailId = ao.find(BlacklistedDomain.class,Query.select().where("SUBJECT != ?", ""));
		 for(BlacklistedDomain domian : checkEmailId)
		{
			// System.out.println(" Testing Subject : " +domian.getSubject());
			// System.out.println(" Testing Domain  : " +domian.getDomain());
			// System.out.println("----------------Testing the Email Id -----------------");
			 if(subject.toLowerCase().contains(domian.getSubject().toLowerCase()))
	        		return true;			 
		}
	      	
      //  System.out.println(" ------ Subject Not found ---------");
		return false;
	}
	
}
