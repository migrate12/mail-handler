package com.equest.jira.plugin.messagehandler;


import java.util.List;
import java.util.Map;
import javax.mail.Message;
import javax.mail.MessagingException;
import org.apache.log4j.Logger;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.SummarySystemField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.equest.jira.plugin.utility.CustomFieldUtility;
import com.equest.jira.plugin.utility.ProjectUtil;
import com.equest.jira.plugin.utility.TextUtility;

public class CreateIssueHandler {
	
	 private static Logger log=Logger.getLogger(CreateIssueHandler.class);
	 String priority = null;
	// String issueType = "10002"; 
	 String issueType = ""; 
	 IssueManager issueManager = ComponentAccessor.getIssueManager();
	 MessageProcessor messageProcessor = new MessageProcessor(); 
//	 ProjectUtil projectUtil = new ProjectUtil();
	 CustomFieldUtility customFieldUtility = new CustomFieldUtility();
	 ProjectUtil projectUtil= new ProjectUtil();
	 String summary ="";
	
	
	public Boolean createIssue(Message message, MessageHandlerContext context,MessageUserProcessor messageUserProcessor, String projectKey,String issueType) throws MessagingException
	{	
		
		Map<String, Object> rawData = messageProcessor.segregateData(message);
		
	//	System.out.println("----------------- createIssue Method -----------------");       
     
      //  final User reporter = messageUserProcessor.getAuthorFromSender(message);
  //      User reporter = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Project projectObject = null;
        if(projectKey != null)
        	projectObject = ComponentAccessor.getProjectManager().getProjectObjByKey(projectKey);
        else
        	projectObject = ComponentAccessor.getProjectManager().getProjectObjByKey("CM");
        
        if(issueType != null)
        {
        	Long temp = projectUtil.getIssueTypeId(projectObject.getId(), issueType);
        	//System.out.println(" Id of Issue type : " +temp);
        	issueType = temp.toString();
        }
        else
        	issueType = "10002"; 
        
        User reporter = projectObject.getProjectLead().getDirectoryUser();
        
        
       
		MutableIssue issueObject = ComponentAccessor.getIssueFactory().getIssue();
        issueObject.setProjectObject(projectObject);
      
        issueObject.setReporter(reporter);
        issueObject.setAssignee(reporter);
        
      
        
       
        if(rawData.containsKey("Summary"))
		{
        	summary = TextUtility.cleanInvalidCharacters((rawData.get("Summary")).toString());
        	if (summary.length() > SummarySystemField.MAX_LEN.intValue())
            {
                context.getMonitor().info("Truncating summary field because it is too long: " + summary);
                summary = summary.substring(0, SummarySystemField.MAX_LEN.intValue() - 3) + "...";
            }
        	issueObject.setSummary(summary);
		}
        
        if(rawData.containsKey("Description"))
		{
        	//issueObject.setDescription(((String) rawData.get("Description")).replaceAll("\\p{C}", ""));
        	//	issueObject.setDescription(((String) rawData.get("Description")));
        	//	issueObject.setDescription(((String) rawData.get("Description")).replaceAll("[^\\u0000-\\uFFFF]", "\uFFFD"));
        	issueObject.setDescription(TextUtility.cleanInvalidCharacters(((rawData.get("Description")).toString())));
		}
     //   
        if(rawData.containsKey("Project"))
		{
        	issueObject.setProjectObject(ComponentAccessor.getProjectManager().getProjectObjByKey((String)rawData.get("Project")));
		}
        
      
	//	issueObject.setLabels(test);
        
     
        // incomplete code
      //  System.out.println("---------------Issue Type ---------- : " +(String) rawData.get("Issue Type"));
        if(rawData.containsKey("Issue Type"))
		{
        	Long temp = projectUtil.getIssueTypeId(projectObject.getId(), (String) rawData.get("Issue Type"));
        	System.out.println(" Id of Issue type : " +temp);
        	issueType = temp.toString();
        	issueObject.setIssueTypeId(issueType);
		}
        else
        {
        	  issueObject.setIssueTypeId(issueType);
        }
        
        if(rawData.containsKey("Priority"))
		{
        	issueObject.setPriorityId((String) rawData.get("Priority"));
		}
        else
        {
        	priority = projectUtil.getDefaultSystemPriority();
        	issueObject.setPriorityId((String) rawData.get(priority));
        }
        
              

       
        /*
        // Give the CustomFields a chance to set their default values JRA-11762
        List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
        for (CustomField customField : customFieldObjects)
        {
            issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
        }*/

      //  fields.put(WorkflowFunctionUtils.ORIGINAL_ISSUE_KEY, originalIssue);
   //     System.out.println("-----------------  fields -----------------");
   //     Map<String, Object> temp = new HashMap<String, Object>();
    //    IssueService issueService = ComponentAccessor.getIssueService();
        try 
        {	
        	System.out.println("----------------- Creating Issue ----- : ");
        	
        	 //---------------------------------
        	
        	//   Map<String, Object> fields = new HashMap<String, Object>();
            //   fields.put("issue", issueObject);
            // MutableIssue originalIssue = ComponentAccessor.getIssueManager().getIssueObject(issueObject.getId());

             
               
               //---------------------------------
        	
        	 List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
             for (CustomField customField : customFieldObjects)
             {
             	if((customField.getName()).equals("To") || (customField.getName()).equals("Cc") )
             	{
             		if((customField.getName()).equals("Cc"))
                 	{
                 		if(rawData.containsKey("Cc"))
                 			issueObject.setCustomFieldValue(customField, rawData.get("Cc").toString());
                 		else
                 			issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
                 	}
             		else
             		{
             			if(rawData.containsKey("To"))
                 			issueObject.setCustomFieldValue(customField, rawData.get("To").toString());
                 		else
                 			issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
             		}
             	}
             	else
             		issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
             }     	
        	
           // fields.put(WorkflowFunctionUtils.ORIGINAL_ISSUE_KEY, originalIssue);
             
			final Issue issue = context.createIssue(reporter, issueObject);
			
		    customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
		    Boolean flag = false;
		    for (CustomField customField : customFieldObjects)
		     {
		    	String cfType =customFieldUtility.getCustomFieldType(customField);
		        if(rawData.containsKey(customField.getName()) &&  (cfType.equals("SelectCFType") || cfType.equals("MultiSelectCFType")))
		         {
		           	Option option = customFieldUtility.getOption(customField, (rawData.get(customField.getName()).toString()));    
		           	System.out.println("Option Value : " +option.getValue());
		           	if(option != null)
		           	{
		           		customFieldUtility.saveValue(issueObject, option,customField);      
		           		flag = true;
		            }	    
		         }
		     }
		    
		    FileAttachmentHandler fileAttachmentHandler = new FileAttachmentHandler();
   		 	fileAttachmentHandler.addAttachementIfAny(message, issue,context);
   		 	CommentHandler  newCommentHandler = new CommentHandler();
   		 	newCommentHandler.updateMailTrail(issue, message);
		//    message.getFileName();;
		    
		   // context.createAttachment();
		    
	//	    CustomField toCustomeField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("To");
//		    if(toCustomeField != null)
//		    {
	//	    	toCustomeField.updateValue(arg0, arg1, arg2, arg3)
	//	    }
		    if(flag)
		    	ComponentAccessor.getIssueIndexManager().reIndex(issue);
			System.out.println("----------------- Issue Created : " +issue.getKey());
			return true;
		}
        catch (CreateException e)
        {
        	log.error("---------------Error while creating Issue--------------" +summary);
        	e.printStackTrace();
		} catch (IndexException e) {
			log.error("---------------Error while creating Issue--------------" +summary);
			e.printStackTrace();
		}
        catch (FieldLayoutStorageException e) 
        {	
        	log.error("---------------Error while creating Issue--------------" +summary);
			e.printStackTrace();
		} 
       return false;
	}
	
	public Boolean createIssue(Message message, MessageHandlerContext context,MessageUserProcessor messageUserProcessor, String projectKey,String issueType, String subject) throws MessagingException
	{	
		
		Map<String, Object> rawData = messageProcessor.segregateData(message);
		
	//	System.out.println("----------------- createIssue Method -----------------");       
     
      //  final User reporter = messageUserProcessor.getAuthorFromSender(message);
  //      User reporter = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Project projectObject = null;
        if(projectKey != null)
        	projectObject = ComponentAccessor.getProjectManager().getProjectObjByKey(projectKey);
        else
        	projectObject = ComponentAccessor.getProjectManager().getProjectObjByKey("CM");
        
        if(issueType != null)
        {
        	Long temp = projectUtil.getIssueTypeId(projectObject.getId(), issueType);
        	//System.out.println(" Id of Issue type : " +temp);
        	issueType = temp.toString();
        }
        else
        	issueType = "10002"; 
        
        User reporter = projectObject.getProjectLead().getDirectoryUser();
        
        
       
		MutableIssue issueObject = ComponentAccessor.getIssueFactory().getIssue();
        issueObject.setProjectObject(projectObject);
      
        issueObject.setReporter(reporter);
        issueObject.setAssignee(reporter);
        
        if(subject != null)
		{
        	summary = TextUtility.cleanInvalidCharacters(subject);
        	if (summary.length() > SummarySystemField.MAX_LEN.intValue())
            {
                context.getMonitor().info("Truncating summary field because it is too long: " + summary);
                summary = summary.substring(0, SummarySystemField.MAX_LEN.intValue() - 3) + "...";
            }
        	issueObject.setSummary(summary);
		}
        
        if(rawData.containsKey("Description"))
		{
        	//issueObject.setDescription(((String) rawData.get("Description")).replaceAll("\\p{C}", ""));
        	//	issueObject.setDescription(((String) rawData.get("Description")));
        	//	issueObject.setDescription(((String) rawData.get("Description")).replaceAll("[^\\u0000-\\uFFFF]", "\uFFFD"));
        	issueObject.setDescription(TextUtility.cleanInvalidCharacters(((String) rawData.get("Description"))));
		}
     //   
        if(rawData.containsKey("Project"))
		{
        	issueObject.setProjectObject(ComponentAccessor.getProjectManager().getProjectObjByKey((String)rawData.get("Project")));
		}
        
      
	//	issueObject.setLabels(test);
        
     
        // incomplete code
      //  System.out.println("---------------Issue Type ---------- : " +(String) rawData.get("Issue Type"));
        if(rawData.containsKey("Issue Type"))
		{
        	Long temp = projectUtil.getIssueTypeId(projectObject.getId(), (String) rawData.get("Issue Type"));
        	System.out.println(" Id of Issue type : " +temp);
        	issueType = temp.toString();
        	issueObject.setIssueTypeId(issueType);
		}
        else
        {
        	  issueObject.setIssueTypeId(issueType);
        }
        
        if(rawData.containsKey("Priority"))
		{
        	issueObject.setPriorityId((String) rawData.get("Priority"));
		}
        else
        {
        	priority = projectUtil.getDefaultSystemPriority();
        	issueObject.setPriorityId((String) rawData.get(priority));
        }
        
              

       
        /*
        // Give the CustomFields a chance to set their default values JRA-11762
        List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
        for (CustomField customField : customFieldObjects)
        {
            issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
        }*/

      //  fields.put(WorkflowFunctionUtils.ORIGINAL_ISSUE_KEY, originalIssue);
   //     System.out.println("-----------------  fields -----------------");
   //     Map<String, Object> temp = new HashMap<String, Object>();
    //    IssueService issueService = ComponentAccessor.getIssueService();
        try 
        {	
        	System.out.println("----------------- Creating Issue ----- : ");
        	
        	 //---------------------------------
        	
        	//   Map<String, Object> fields = new HashMap<String, Object>();
            //   fields.put("issue", issueObject);
            // MutableIssue originalIssue = ComponentAccessor.getIssueManager().getIssueObject(issueObject.getId());

             
               
               //---------------------------------
        	
        	 List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
             for (CustomField customField : customFieldObjects)
             {
             	if((customField.getName()).equals("To") || (customField.getName()).equals("Cc") )
             	{
             		if((customField.getName()).equals("Cc"))
                 	{
                 		if(rawData.containsKey("Cc"))
                 			issueObject.setCustomFieldValue(customField, rawData.get("Cc").toString());
                 		else
                 			issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
                 	}
             		else
             		{
             			if(rawData.containsKey("To"))
                 			issueObject.setCustomFieldValue(customField, rawData.get("To").toString());
                 		else
                 			issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
             		}
             	}
             	else
             		issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
             }     	
        	
           // fields.put(WorkflowFunctionUtils.ORIGINAL_ISSUE_KEY, originalIssue);
             
			final Issue issue = context.createIssue(reporter, issueObject);
			
		    customFieldObjects = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issueObject);
		    Boolean flag = false;
		    for (CustomField customField : customFieldObjects)
		     {
		    	String cfType =customFieldUtility.getCustomFieldType(customField);
		        if(rawData.containsKey(customField.getName()) &&  (cfType.equals("SelectCFType") || cfType.equals("MultiSelectCFType")))
		         {
		           	Option option = customFieldUtility.getOption(customField, (rawData.get(customField.getName()).toString()));    
		           	System.out.println("Option Value : " +option.getValue());
		           	if(option != null)
		           	{
		           		customFieldUtility.saveValue(issueObject, option,customField);      
		           		flag = true;
		            }	    
		         }
		     }
		    
		    FileAttachmentHandler fileAttachmentHandler = new FileAttachmentHandler();
   		 	fileAttachmentHandler.addAttachementIfAny(message, issue,context);
   		 	CommentHandler  newCommentHandler = new CommentHandler();
   		 	newCommentHandler.updateMailTrail(issue, message);
		//    message.getFileName();;
		    
		   // context.createAttachment();
		    
	//	    CustomField toCustomeField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("To");
//		    if(toCustomeField != null)
//		    {
	//	    	toCustomeField.updateValue(arg0, arg1, arg2, arg3)
	//	    }
		    if(flag)
		    	ComponentAccessor.getIssueIndexManager().reIndex(issue);
			System.out.println("----------------- Issue Created : " +issue.getKey());
			return true;
		}
        catch (CreateException e)
        {
        	log.error("---------------Error while creating Issue--------------" +summary);
        	e.printStackTrace();
		} catch (IndexException e) {
			log.error("---------------Error while creating Issue--------------" +summary);
			e.printStackTrace();
		}
        catch (FieldLayoutStorageException e) 
        {	
        	log.error("---------------Error while creating Issue--------------" +summary);
			e.printStackTrace();
		} 
       return false;
	}
	
}
