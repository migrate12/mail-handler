package com.equest.jira.plugin.messagehandler;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.core.util.collection.EasyList;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.util.SecureRandomStringUtils;
import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.user.UserEventType;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.mail.MailLoggingManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.service.util.handler.MessageHandler;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageHandlerErrorCollector;
import com.atlassian.jira.service.util.handler.MessageHandlerExecutionMonitor;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.web.util.FileNameCharacterCheckerUtil;
import com.atlassian.mail.MailUtils;
import com.google.common.collect.Lists;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;

import javax.annotation.Nullable;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileAttachmentHandler {
	
	
	public void addAttachementIfAny(final Message message, final Issue issue,final MessageHandlerContext context)
	{
		System.out.println("----------------- Creating File Attachment  : " +issue.getKey());
		try 
		{
			createAttachmentsForMessage(message, issue,context);
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (MessagingException e) 
		{
			e.printStackTrace();
		}
		  System.out.println("----------------- File Attachment Created : " +issue.getKey());
	}

	private static Logger log=Logger.getLogger(AttachmentHandler.class);
	
    /**
     * filename used if one cannot be determined from an attached message
     */
    private static final String ATTACHED_MESSAGE_FILENAME = "attachedmessage";

    /**
     * The default filename assigned to attachments that do not contain a filename etc
     */
    private final static String DEFAULT_BINARY_FILE_NAME = "binary.bin";
    protected static final String HEADER_MESSAGE_ID = "message-id";
    private static final char INVALID_CHAR_REPLACEMENT = '_';
    protected static final String HEADER_IN_REPLY_TO = "in-reply-to";
    private static final FileNameCharacterCheckerUtil fileNameCharacterCheckerUtil = new FileNameCharacterCheckerUtil();
    
	
	 /**
     * Loops through all the {@link Part}s, and for each one of type {@link Part#ATTACHMENT}, call {@link
     * #createAttachmentWithPart(javax.mail.Part, com.atlassian.crowd.embedded.api.User, Issue, MessageHandlerContext)}.
     *
     * @param message The multipart message to search for attachments in
     * @param issue The issue to create attachments in
     * @return a collection of change items, one for each attachment added. If no attachments are added, returns and
     *         empty collection.
     * @throws IOException If there is a problem creating the attachment
     * @throws MessagingException If there is a problem reading the message
     */
    protected Collection<ChangeItemBean> createAttachmentsForMessage(final Message message, final Issue issue,
            final MessageHandlerContext context) throws IOException, MessagingException
    {
        final Collection<ChangeItemBean> attachmentChangeItems = Lists.newArrayList();
        AttachmentHandler attachmentHandler = null;
        Boolean attachmentsAllowed = false;
        final User reporter = issue.getCreator();
        if(reporter != null)
        	attachmentsAllowed = true ;
        	
        if (attachmentsAllowed)
        {
            attachmentHandler = new AttachmentHandler()
            {
                @Override
                public void handlePart(Part part, Message containingMessage) throws IOException, MessagingException
                {
                    if (shouldAttach(part, containingMessage)) {
                        final ChangeItemBean changeItemBean = createAttachmentWithPart(part, reporter, issue, context);
                        if (changeItemBean != null)
                        {
                            attachmentChangeItems.add(changeItemBean);
                        }
                    }
                }

                @Override
                public void summarize() {}
            };
        }
        /*
        else
        {
            attachmentHandler = new AttachmentHandler()
            {
                private final List<String> skippedFiles = Lists.newArrayList();
                @Override
                public void handlePart(Part part, Message containingMessage) throws IOException, MessagingException
                {
                    if (shouldAttach(part, containingMessage)) {
                        final String filenameForAttachment = getFilenameForAttachment(part);
                        if (StringUtils.isNotBlank(filenameForAttachment))
                        {
                            // if filename is blank the file would not be attached anyway - spaghetti FTW
                            skippedFiles.add(filenameForAttachment);
                        }
                    }
                }

                @Override
                public void summarize()
                {
                    if (!skippedFiles.isEmpty()) {
                        final String message = !attachmentsAllowed
                                ? getI18nBean().getText("jmp.handler.attachments.disabled")
                                : getI18nBean().getText("jmp.handler.attachments.no.create.permission", reporter.getName(), issue.getProjectObject().getKey());
                        if (log.isDebugEnabled()) {
                            log.debug(message + StringUtils.join(skippedFiles, ", "));
                        }
                        final String comment = message + "\n - " + StringUtils.join(skippedFiles, "\n - ");
                        context.createComment(issue, reporter, comment, false);
                    }
                }
            };
        }
*/
        final Object messageContent = message.getContent();
        if (messageContent instanceof Multipart)
        {
            handleMultipart((Multipart) messageContent, message, attachmentHandler);
        }
        //JRA-12123: Message is not a multipart, but it has a disposition of attachment.  This means that
        //we got a message with an empty body and an attachment.  We'll ignore inline.
        else if (Part.ATTACHMENT.equalsIgnoreCase(message.getDisposition()))
        {
            log.debug("Trying to add attachment to issue from attachment only message.");

            attachmentHandler.handlePart(message, null);
        }

        attachmentHandler.summarize();
        return attachmentChangeItems;
    }
    
    
    /**
     * Create an attachment for a particular mime-part.  The BodyPart must be of type {@link Part#ATTACHMENT}.
     *
     * @param part part of disposition {@link javax.mail.Part#ATTACHMENT} to create the attachment from
     * @param reporter issue reporter
     * @param issue issue to create attachments in
     * @return A {@link ChangeItemBean} representing the added attachment, or null if no attachment was created
     * @throws IOException If there is a problem creating the attachment in the filesystem
     */
    protected ChangeItemBean createAttachmentWithPart(final Part part, final User reporter, final Issue issue,
            MessageHandlerContext context) throws IOException
    {
        try
        {
            final String contentType = MailUtils.getContentType(part);
            final String rawFilename = part.getFileName();
            String filename = getFilenameForAttachment(part);

            final File file = getFileFromPart(part, (issue != null ? issue.getKey() : "null"));

            if (log.isDebugEnabled())
            {
                log.debug("part=" + part);
                log.debug("Filename=" + filename + ", content type=" + contentType + ", content=" + part.getContent());
            }

            filename = renameFileIfInvalid(filename, issue, reporter, context);

            final ChangeItemBean cib = context.createAttachment(file, filename, contentType, reporter, issue);
            if (cib != null)
            {
                log.debug("Created attachment " + rawFilename + " for issue " + issue.getKey());
                return cib;
            }
            else
            {
                log.debug("Encountered an error creating the attachment " + rawFilename + " for issue " + issue.getKey());
                return null;
            }
        }
        catch (final Exception e)
        {
            log.error("Exception while creating attachment for issue " + (issue != null ? issue.getKey() : "null") + ": " + e, e);
            throw new IOException(e.getMessage(), e);
        }
    }
    
    /**
     * Handy method which takes a number of strategies when attempting to give a filename for a particular part. The
     * filename may already be present in the part or may need to be formulated or composed from other identifies within
     * the part (such as a subject, content type etc).
     *
     * @param part The part being tested.
     * @return The filename for the attachment or null if one was not present.
     * @throws MessagingException relays any MessagingException thrown by a lower layer such as java mail
     * @throws IOException relays any IOExceptions
     */
    protected String getFilenameForAttachment(final Part part) throws MessagingException, IOException
    {
        String filename = getFilenameFromPart(part);
        if (null == filename)
        {
            if (MailUtils.isPartMessageType(part))
            {
                filename = getFilenameFromMessageSubject(part);
            }
            else if (MailUtils.isPartInline(part))
            {
                filename = getFilenameFromContentType(part);
            }
        }

        // double check that filename extracting worked!
        if (null != filename)
        {
            if (StringUtils.isBlank(filename))
            {
                final String message = "Having found a filename(aka filename is not null) filename should not be an empty string, but is...";
                log.warn(message);

                // since empty string is invalid, return null and let a name be default or generated name be used instead.
                filename = null;
            }
        }

        return filename;
    }
    
    /**
     * Retrieves the filename from a mail part and MIME decodes it if necessary.
     *
     * @param part a mail part - may or may not have a file name.
     * @return the file name set on the part, or null.
     * @throws MessagingException if retrieving the file name fails.
     * @throws IOException if doing the MIME decoding fails.
     */
    private String getFilenameFromPart(final Part part) throws MessagingException, IOException
    {
        String filename = part.getFileName();
        if (null != filename)
        {
            filename = MailUtils.fixMimeEncodedFilename(filename);
        }
        return filename;
    }

    private String getFilenameFromMessageSubject(final Part part) throws MessagingException, IOException
    {
        // JRA-15133: determine filename from subject line of the message
        final Message message = (Message) part.getContent();
        String filename = message.getSubject();
        if (StringUtils.isBlank(filename))
        {
            // if no subject, use Message-ID
            try
            {
                filename = getMessageId(message);
            }
            catch (final ParseException e)
            {
                // no Message-ID, use constant
                filename = ATTACHED_MESSAGE_FILENAME;
            }
        }

        return filename;
    }

    /**
     * Allocates or composes a filename from a part, typically this is done by massaging the content type into a
     * filename.
     *
     * @param part The part
     * @return The composed filename
     * @throws MessagingException May be thrown by javamail
     * @throws IOException May be thrown by javamail.
     */
    private String getFilenameFromContentType(final Part part) throws MessagingException, IOException
    {
        String filename = DEFAULT_BINARY_FILE_NAME;

        final String contentType = MailUtils.getContentType(part);
        final int slash = contentType.indexOf("/");
        if (-1 != slash)
        {
            final String subMimeType = contentType.substring(slash + 1);

            // if its not a binary attachment convert the content type into a filename image/gif becomes image-file.gif
            if (!subMimeType.equals("bin"))
            {
                filename = contentType.substring(0, slash) + '.' + subMimeType;
            }
        }

        return filename;
    }
    
    /**
     * Returns the Message-ID for a given message.
     *
     * @param message a message
     * @return the Message-ID
     * @throws ParseException if the Message-ID header was not present
     * @throws javax.mail.MessagingException if javamail complains
     */
    String getMessageId(final Message message) throws MessagingException, ParseException
    {
        // Note: we get an array because Message-ID is an arbitrary header, but really there can be only one Message-ID
        // value (if it is present)
        final String[] originalMessageIds = message.getHeader(HEADER_MESSAGE_ID);
        if ((originalMessageIds == null) || (originalMessageIds.length == 0))
        {
            final String msg = "Could not retrieve Message-ID header from message: " + message;
            log.debug(msg);
            throw new ParseException(msg);
        }
        return originalMessageIds[0];
    }
    
    /**
     * Replaces all invalid characters in the filename using {@link FileNameCharacterCheckerUtil#replaceInvalidChars(String,
     * char)} with {@link #INVALID_CHAR_REPLACEMENT} as the replacement character.
     *
     *
     * @param filename filename to check if its valid
     * @param issue issue the file is to be attached
     * @param reporter the author of the comment to add to the issue if the filename is invalid
     * @return <li>if filename is null, returns null</li> <li>if its valid, returns filename</li> <li>if its invalid,
     *         returns filename with all invalid characters replaced with {@link #INVALID_CHAR_REPLACEMENT}</li>
     */
    protected String renameFileIfInvalid(final String filename, final Issue issue, final User reporter,
            MessageHandlerContext context)
    {
        if (filename == null)
        {
            //let the attachmentManager handle the null filename when creating.
            return null;
        }

        //replace any invalid characters with the INVALID_CHAR_REPLACEMENT character
        final String replacedFilename = fileNameCharacterCheckerUtil.replaceInvalidChars(filename, INVALID_CHAR_REPLACEMENT);
        //if the filename has changed then add a comment to the issue to say it has been changed because of invalid characters
        if (!filename.equals(replacedFilename))
        {
            if (log.isDebugEnabled())
            {
                log.debug("Filename was invalid: replacing '" + filename + "' with '" + replacedFilename + "'");
            }
            return replacedFilename;
        }
        return filename;
    }
    
    private interface AttachmentHandler
    {
        void handlePart(Part part, Message containingMessage) throws IOException, MessagingException;
        void summarize();
    }
    
    protected File getFileFromPart(final Part part, final String issueKey)
            throws IOException, MessagingException, GenericEntityException
    {
        File tempFile = null;
        try
        {
            tempFile = File.createTempFile("tempattach", "dat");
            final FileOutputStream out = new FileOutputStream(tempFile);

            try
            {
                part.getDataHandler().writeTo(out);
            }
            finally
            {
                out.close();
            }
        }
        catch (final IOException e)
        {
            log.error("Problem reading attachment from email for issue " + issueKey, e);
        }
        if (tempFile == null)
        {
            throw new IOException("Unable to create file?");
        }
        return tempFile;
    }
    
    /**
     * This method determines if a particular part should be included added as an attachment to an issue.
     *
     * @param part the part to potentially attach
     * @param containingMessage the message which contained the part - may be null
     * @return true if the part should be attached; false otherwise
     * @throws java.io.IOException if javamail complains
     * @throws javax.mail.MessagingException if javamail complains
     */
    final protected boolean shouldAttach(final Part part, final Message containingMessage)
            throws MessagingException, IOException
    {
        Assertions.notNull("part", part);

        boolean attach;

        if (log.isDebugEnabled())
        {
            log.debug("Checking if attachment should be added to issue:");
            log.debug("\tContent-Type: " + part.getContentType());
            log.debug("\tContent-Disposition: " + part.getDisposition());
        }

        if (MailUtils.isPartMessageType(part) && (null != containingMessage))
        {
            log.debug("Attachment detected as a rfc/822 message.");
            attach = attachMessagePart(part, containingMessage);
        }
        else if (isPartAttachment(part))
        {
            log.debug("Attachment detected as an \'Attachment\'.");
            attach = attachAttachmentsParts(part);
        }
        else if (MailUtils.isPartInline(part))
        {
            log.debug("Attachment detected as an inline element.");
            attach = attachInlineParts(part);
        }
        else
        {
            attach = false;
        }

        if (log.isDebugEnabled())
        {
            if (attach)
            {
                log.debug("Attachment was added to issue");
            }
            else
            {
                log.debug("Attachment was ignored.");
            }
        }

        return attach;
    }
    
    /**
     * JRA-15133: if this part is an attached message, skip it if: <ul> <li>the option to ignore attached messages is
     * set to true, OR</li> <li>this message is in reply to the attached one (redundant info), OR</li> <li>if the
     * message is not in reply to the attached one, skip if the content is empty</li> </ul>
     * <p/>
     * This is required to handle the behaviour of some mail clients (e.g. Outlook) who, when replying to a message,
     * include the entire original message as an attachment with content type "message/rfc822". In these cases, the
     * attached message is redundant information, so we skip it.
     *
     * @param messagePart the Message part (already known to be of message content type)
     * @param containingMessage the original message which contains messagePart
     * @return true if the part should be attached, false otherwise
     * @throws java.io.IOException if javamail complains
     * @throws javax.mail.MessagingException if javamail complains
     */
    protected boolean attachMessagePart(final Part messagePart, final Message containingMessage)
            throws IOException, MessagingException
    {
        boolean keep = false;
        boolean shouldIgnoreEmailMessageAttachments =true;

        // only keep message parts if we are not ignoring them
        if (!shouldIgnoreEmailMessageAttachments)
        {
            // .. and the message part is not being replied to by this message
            if (!isReplyMessagePart(messagePart, containingMessage))
            {
                // .. and the message part is not empty
                keep = !MailUtils.isContentEmpty(messagePart);

                if (!keep && log.isDebugEnabled())
                {
                    log.debug("Attachment not attached to issue: Message is empty.");
                }
            }
            else
            {
                log.debug("Attachment not attached to issue: Detected as reply.");
            }
        }
        else
        {
            log.debug("Attachment not attached to issue: Message attachment has been disabled.");
        }

        return keep;
    }

    /**
     * JRA-15670: if this part is contained within a multipart/related message, keep it, as it may be a legitimate
     * attachment, but without the content disposition set to a sensible value (e.g. when using Outlook 2007 and Rich
     * Text format).
     *
     * @param part the part contained within the related message
     * @return true if the part should be attached, false otherwise
     * @throws java.io.IOException if javamail complains
     * @throws javax.mail.MessagingException if javamail complains
     */
    protected boolean attachRelatedPart(final Part part) throws IOException, MessagingException
    {
        return !MailUtils.isContentEmpty(part);
    }
    
    private boolean isPartAttachment(Part part) throws MessagingException
    {
        return MailUtils.isPartAttachment(part)
                || (StringUtils.isNotBlank(part.getFileName()) && StringUtils.isNotBlank(part.getContentType()) && part.getSize() > 0);
    }
    
    /**
     * Only attach an attachment part if it's content is not empty and if it is not a signature part.
     *
     * @param part a mail part - assumed to have attachment disposition
     * @return whether or not this inline part should be attached.
     * @throws MessagingException if Content-Type checks fail
     * @throws IOException if content checks fail
     */
    protected boolean attachAttachmentsParts(final Part part) throws MessagingException, IOException
    {
        return !MailUtils.isContentEmpty(part) && !MailUtils.isPartSignaturePKCS7(part);
    }
    
    /**
     * Only attach an inline part if it's content is not empty and if it is not a signature part.
     *
     * @param part a mail part - assumed to have inline disposition
     * @return whether or not this inline part should be attached.
     * @throws MessagingException if Content-Type checks fail
     * @throws IOException if content checks fail
     */
    protected boolean attachInlineParts(final Part part) throws MessagingException, IOException
    {
        return !MailUtils.isContentEmpty(part) && !MailUtils.isPartSignaturePKCS7(part);
    }
    
    private void handleMultipart(final Multipart multipart, Message message, AttachmentHandler attachmentHandler) throws MessagingException, IOException
    {
        for (int i = 0, n = multipart.getCount(); i < n; i++)
        {
            if (log.isDebugEnabled())
            {
                log.debug(String.format("Adding attachments for multi-part message. Part %d of %d.", i + 1, n));
            }

            final BodyPart part = multipart.getBodyPart(i);

            // there may be non-attachment parts (e.g. HTML email) - fixes JRA-1842
            final Object partContent = part.getContent();
            if (partContent instanceof Multipart)
            {
                // Found another multi-part - process it and collection all change items.
                handleMultipart((Multipart) partContent, message, attachmentHandler);
            }
            else
            {
                // JRA-15133: if this part is an attached message, skip it if:
                // * the option to ignore attached messages is set to true, OR
                // * this message is in reply to the attached one (redundant info)
                // Note: this is now covered by the shouldAttach() method
                attachmentHandler.handlePart(part, message);
            }
        }
    }
    
    /**
     * Helper which tests if the incoming part is a reply to the containing message
     *
     * @param messagePart The part being tested
     * @param containingMessage The container message
     * @return True if the part is definitely a reply to the message, otherwise returns false;
     * @throws java.io.IOException if javamail complains
     * @throws javax.mail.MessagingException if javamail complains
     */
    private boolean isReplyMessagePart(final Part messagePart, final Message containingMessage)
            throws IOException, MessagingException
    {
        boolean replyMessage;

        try
        {
            replyMessage = isMessageInReplyToAnother(containingMessage, (Message) messagePart.getContent());
        }
        catch (final ParseException e)
        {
            log.debug("Can't tell if the message is in reply to the attached message -- will attach it in case");
            replyMessage = false;
        }

        return replyMessage;
    }
   
    /**
     * Checks if the containing message is "In-Reply-To" to the attached message. This is done through checking standard
     * email headers as specified in RFC-822.
     *
     * @param containingMessage a message, which may be the reply.
     * @param attachedMessage another message which may be the original
     * @return true if the first message is in reply to the second.
     * @throws ParseException if there was an error in retrieving the required message headers
     * @throws javax.mail.MessagingException if javamail complains
     */
    private boolean isMessageInReplyToAnother(final Message containingMessage, final Message attachedMessage)
            throws MessagingException, ParseException
    {
        // Note: we are using the fact that most common mail clients use In-Reply-To to reference the Message-ID of the
        // message being replied to. This is "defacto standard" but is not guaranteed by the spec.
        final String attachMessageId = getMessageId(attachedMessage);
        final String[] replyToIds = containingMessage.getHeader(HEADER_IN_REPLY_TO);

        if (log.isDebugEnabled())
        {
            log.debug("Checking if attachment was reply to containing message:");
            log.debug("\tAttachment mesage id: " + attachMessageId);
            log.debug("\tNew message reply to values: " + Arrays.toString(replyToIds));
        }

        if (replyToIds != null)
        {
            for (final String id : replyToIds)
            {
                if ((id != null) && id.equalsIgnoreCase(attachMessageId))
                {
                    return true;
                }
            }
        }
        return false;
    }
  
//*/
    

}
