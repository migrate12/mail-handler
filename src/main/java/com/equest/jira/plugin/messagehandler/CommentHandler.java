package com.equest.jira.plugin.messagehandler;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.StringUtils;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.atlassian.mail.MailUtils;
import com.equest.jira.plugin.utility.CustomFieldUtility;
import com.equest.jira.plugin.utility.TextUtility;


public class CommentHandler {
	

	public String getEmailBody(Message message) throws MessagingException
	{
	     return TextUtility.cleanInvalidCharacters(MailUtils.getBody(message));
	}
	 
	public Boolean AddComment(Message message, MessageHandlerContext context,MessageUserProcessor messageUserProcessor,String issueKey)  throws MessagingException
	{
		
		Issue issue = ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase(issueKey);
		

	    if (issue == null)
	    {
	       return false; // returning false means that we were unable to handle this message. It may be either
	           // forwarded to specified address or left in the mail queue (if forwarding not enabled)
	    }
	
	    
	    final User sender = issue.getProjectObject().getProjectLead().getDirectoryUser(); 
	    //messageUserProcessor.getAuthorFromSender(message);
	//    if (sender == null) 
	//    {
	//       context.getMonitor().error("Message sender(s) '" + StringUtils.join(MailUtils.getSenders(message), ",")
	//                    + "' do not have corresponding users in JIRA. Message will be ignored");
//	        return false;
//	    }
	     
	//    System.out.println("-------------------------------------------------------------");
	    String emailId="";
	 
	   Address[] t = message.getFrom();
	   for(Address i : t)
	   {
		   
		   emailId = emailId + i;
	//	   System.out.println(" You : " +emailId);
	   }

	    
	    final String body = getupdatedMail(TextUtility.cleanInvalidCharacters(MailUtils.getBody(message).toString())) + "\r\n " +" Commented by " + emailId;
	//    System.out.println(" GOT Mail Message : " +body); 
	//    final StringBuilder commentBody = new StringBuilder(message.getSubject());
	    final StringBuilder commentBody = new StringBuilder();
	        
	    if (body != null)
	    {
	        commentBody.append("\n").append(StringUtils.abbreviate(body, 100000)); // let trim too long bodies
	    }
	    context.createComment(issue, sender, commentBody.toString(), false);
	    
	    // returning true means that we have handled the message successfully. It means it will be deleted next.
	    return true; 
	  
	 }
	
	public boolean isAutoSubmitted(final Message message) throws MessagingException
    {

        final String[] autoSub = message.getHeader("Auto-Submitted");
        if (autoSub != null)
        {
            for (final String auto : autoSub)
            {
                if (!"no".equalsIgnoreCase(auto))
                {
                    return true;
                }
            }
        }
        return false;
    }
	
	public String getEmailId(Address[] address)
	{
		String temp = InternetAddress.toString(address);
		if(temp != null)
			return temp;
		else
			return "";
	}
	
	
	
	
	public boolean updateMailTrail(Issue issue, Message message)
	{
		try {
		 	System.out.println("-----------------Starting Mail Trail ------------------------");
		//    AoUtility aoUtility = new AoUtility(ao);
		    String mailInfo = "" ;	// get info foe custome field.
		    		
		    		//aoUtility.getMailTrail(issue.getKey());
		    Address[] to = message.getRecipients(Message.RecipientType.TO);
			Address[] cc = message.getRecipients(Message.RecipientType.CC);
			Address[] bcc = message.getRecipients(Message.RecipientType.BCC);
			String toEmailId = null;
			String bccEmailId = null;
			String ccEmailId = null;
			String upadatedMail ="";
			CustomFieldUtility customFieldUtility= new CustomFieldUtility();
			CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("MailTrail");
			mailInfo = customField.getValueFromIssue(issue);
			
			
			if(to!=null)
			{
				toEmailId =getEmailId(to);
				
					upadatedMail ="....................................................................................................................................."
											+"\r\n "  + "From : " +getEmailId(message.getFrom()) 
											+ "\r\n "+ "Sent : "+message.getSentDate().toString() 
											+ "\r\n " + "To : "+toEmailId;
			}
			if(cc!=null)
			{
				ccEmailId =getEmailId(cc);
				upadatedMail = upadatedMail + "\r\n " + "Cc : "+ccEmailId;
			}
			if(bcc!=null)
			{
				bccEmailId =getEmailId(bcc);
				upadatedMail = upadatedMail + "\r\n " + "Bcc : "+bccEmailId;
			}
			 if(mailInfo!= null)
			 {
				 	upadatedMail =  upadatedMail	+ "\r\n "+"Subject : "+TextUtility.cleanInvalidCharacters(message.getSubject().toString())+ 
				 							"\r\n " +getupdatedMail(TextUtility.cleanInvalidCharacters(MailUtils.getBody(message).toString()))
						 						+ "\r\n "+ mailInfo;
				 	System.out.println(" ---------- Updating Mail Trail  -----------");
			//	 	aoUtility.updateMailTrail(issue.getKey(), upadatedMail);
					 if(customField!=null)
						 customFieldUtility.saveTextValue(ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase(issue.getKey()),customField,upadatedMail.toString());
				
				 	System.out.println(" ---------- Updated  Mail Trail  -----------");
			  }
			 else
			 {
				 upadatedMail =  upadatedMail	+ "\r\n "+"Subject : "+ TextUtility.cleanInvalidCharacters(message.getSubject().toString())+ 
						 					"\r\n "+TextUtility.cleanInvalidCharacters(MailUtils.getBody(message).toString());
				 System.out.println(" ---------- Adding New Mail Trail Added -----------");
		//		 aoUtility.addMailTrail(issue.getKey(), upadatedMail);
				 
				 MutableIssue issueObject = ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase(issue.getKey());
				 
				 if(customField!=null)
					 customFieldUtility.saveTextValue(issueObject,customField,upadatedMail.toString());
				 
				 System.out.println(" ---------- New Mail Trail Added -----------");
			 }
		    
			  System.out.println("-----------------End Mail Trail ------------------------");
		} 
		catch (MessagingException e) 
		{
			e.printStackTrace();
		}
		
			  return true;
	}
	
	public String getupdatedMail(String body)
	{

		 String[] lines = body.split("\\r?\\n");
		 String newmail="";
		 
		 for(int i=0 ;i<lines.length;i++)
		 {
			if(!lines[i].contains("From : ")) 
				newmail = newmail +"\r\n "+ lines[i];
			else
		 	{
			  if(lines[i+1].contains("Sent : "))
			  {
				  if(lines[i+2].contains("To : "))
				  {
					 return TextUtility.cleanInvalidCharacters(newmail);
				  }
			  }
		 	}
		}

		 return TextUtility.cleanInvalidCharacters(newmail);
	}
	
	public String getupdatedSentMail(String issueKey,String body)
	{

		System.out.println("======"+  body); 
		String[] lines = body.split("\\r?\\n");
		System.out.println("======"+  lines); 
		 String newmail="";
		 String mailInfo = "" ;
		 CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("MailTrail");
		 MutableIssue issueObject = ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase(issueKey);
		 try{
		 mailInfo = customField.getValueFromIssue(issueObject);	
		 }catch(NullPointerException ex){
			 
		 }
		 
		 for(int i=0 ;i<lines.length;i++)
		 {
			if(!lines[i].contains("From : ")) 
				newmail = newmail +"\r\n "+ lines[i];
			else
		 	{
			  if(lines[i+1].contains("Sent : "))
			  {
				  if(lines[i+2].contains("To : "))
				  {
					 return TextUtility.cleanInvalidCharacters(body);
				  }
			  }
		 	}
		}

		 return TextUtility.cleanInvalidCharacters(newmail + "\r\n "+ mailInfo);
	}
	
	public boolean updateMailTrail(String issueKey,String body)
	{
		CustomFieldUtility customFieldUtility= new CustomFieldUtility();
		CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("MailTrail");
		
		MutableIssue issueObject = ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase(issueKey);
		if(customField!=null)
			customFieldUtility.saveTextValue(issueObject,customField,TextUtility.cleanInvalidCharacters(body.toString()));
		
		return true;
	}

}
