package com.equest.jira.plugin.messagehandler;

import java.util.HashMap;
import java.util.Map;
import com.atlassian.mail.MailUtils;
import com.equest.jira.plugin.utility.TextUtility;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;


public class MessageProcessor {

	 Map<String, Object> rawData = new HashMap<String, Object>();
	 TextUtility textUtility= new TextUtility();
	 
	 public Map<String,Object> segregateData(Message message)
	 {
		 try 
		 {
			String description="";
			String messageBody = MailUtils.getBody(message);
			String[] lines = messageBody.split("\\r?\\n");
			for(int i=0 ;i<lines.length;i++)
			{
				if(i==0 && lines[i].contains("#"))
				{
					String[] temp = lines[i].split("#");
					for(int j=1 ;j<temp.length;j++)
					{
						String[] values = temp[j].split("=");
						if(values.length>=2)
						{
							rawData.put(values[0].trim(), values[1].trim());
						}
					}
				}
				else
				{
					description = description + "\r\n " + lines[i];
				}	
			}
			rawData.put("Description",description.toString());
			rawData.put("Summary", (message.getSubject()).toString());

			
			Address[] to = null;
			to = 	message.getFrom();
			Address[] cc = null;
			cc = message.getRecipients(Message.RecipientType.CC);
			
			if(to !=null)
			{
				String toEmailId="";
				toEmailId =getEmailId(to);
				if(toEmailId!="")
				{
					rawData.put("To", toEmailId);
				}
			}
			if(cc !=null)
			{
				String ccEmailId="";
				ccEmailId = getEmailId(cc);		
				if(ccEmailId!="")
				{
					rawData.put("Cc", ccEmailId);
				}
			}
	
		 } 
		 catch (MessagingException e) 
		 {
			e.printStackTrace();
		}
		 
		 
		 
		 return rawData;
	 }
	 
	 public String getEmailId(Address[] address)
		{
			String emailId = "";
			int count =0;
			 for(Address i : address)
			  {
				 if(count == 0)
					 emailId =  ((InternetAddress) i).getAddress();
				 if(count > 0)
					 emailId = emailId + ","+ ((InternetAddress) i).getAddress();
				count++;
			   }
			return emailId;
		}

}
