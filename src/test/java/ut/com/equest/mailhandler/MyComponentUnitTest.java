package ut.com.equest.mailhandler;

import org.junit.Test;

import com.equest.jira.plugin.component.MyPluginComponent;
import com.equest.jira.plugin.component.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}